var _ = require('lodash');
var async = require('async');
var crypto = require('crypto');
var passport = require('passport');
var User = require('../models/User');
var Room = require('../models/Room');
var Token = require('../models/Token');
var manager = require('../mcu/manager');

/**
 * POST /room
 * Create new room.
 */
exports.createRoom = function(req, res, next) {
  async.waterfall([
    function(done) {
      var room = new Room({
        name: 'New Room'
      });
      room.save(function(err) {
        if (err) {
          return next(err);
        }
        done(err, room);
      });
    },
    function(room, done) {
      if (!req.session.guestId) req.session.guestId = {};
      req.session.guestId[room.id] = 0;
      res.redirect("/room/"+room.id);
    }
  ], function(err) {
    if (err) {
      return next(err);
    }
    res.redirect('/');
  });
};

/**
 * GET /room/:id
 * Join existing room
 */
exports.joinRoom = function(req, res, next) {
  async.waterfall([
    function(done) {
      Room
        .findById(req.params.id)
        .exec(function(err, room) {
          if (err) {
            return next(err);
          }
          if (!room) {
            req.flash('errors', { msg: 'Room not found.' });
            res.redirect('/');
          } else {
            done(err, room);
          }
        });
    }, function(room, done) {
      if (!req.session.guestId) req.session.guestId = {};
      if (!req.session.guestId[room.id]) req.session.guestId[room.id] = (Math.random() * 1000000)|0;
      manager.createToken(room._id, req.session.guestId[room.id], req.sessionID, function(err, token) {
        done(err, room, token);
      });
    }, function(room, token, done) {
      res.render('room/room', {
        id: room._id,
        guestId: req.session.guestId[room.id],
        token: token.encode()
      });
    }
  ], function(err) {
    req.flash('errors', { msg: 'Error joining room.' });
    res.redirect('/');
  });
};

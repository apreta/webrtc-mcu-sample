var rpc = require('./rpc');
var config = require('./../../webrtc-mcu/mcu_config');
var _ = require('lodash');
var Token = require('./../models/Token.js');
var tokenUtil = require('./token');

var id = 0;
var mcuControllers = {};

function findController(ip) {
    return _.find(mcuControllers, function(controller, id) { return controller.ip == ip ;});
}

exports.start = function() {
    rpc.connect();
};

exports.createToken = function(id, guestId, sessionId, callback) {
    "use strict";

    var now = Date.now();

    var controller = _.find(mcuControllers, function(controller, id) { return now - controller.time < 20000; });
    if (!controller) {
        callback(new Error('No video controllers'));
        return;
    }

    Token.findByDetails(id, guestId, sessionId, function(err, result) {
        if (result == null) {
            if (err) console.log('Token search failed:',err);
            var host = (config.mcu.wscert ? "https://" : "http://") + controller.ip + ":" + config.mcu.wsport;
            result = new Token({roomId: id, guestId: guestId, host: host, session: sessionId, role: 'guest'});
            result.save(function(err, result) {
                if (err) console.log("Token update failed");
                callback(err, result);
            });
        } else {
            callback(err, result);
        }
    });
};

exports.deleteToken = function (id, callback) {
    "use strict";

    //tokenRegistry.removeOldTokens();

    Token.findByToken(id, function(err, token) {
        if (err || !token) {
            callback('error');
        } else {
            console.log('Found token ', token.tokenId, 'from room ', token.roomId, ' of host ', token.host);
            callback(token.toObject());
        }
    });
};

exports.addNewMcuController = function(msg, callback) {
    var server = findController(msg.ip);
    if (!server) {
        server = {ip: msg.ip, id: ++id};
        mcuControllers[server.id] = server;
        console.log("Controller " + msg.ip + " registered");
    } else {
        console.log("Controller " + msg.ip + " re-registered");
    }
    server.time = Date.now();

    var result = { id:server.id, publicIP:server.ip };

    callback(result);
};

exports.keepAlive = function(id, callback) {
    var server = mcuControllers[id];
    if (server) {
        server.time = Date.now();
    }
    var result = server ? "ok" : "whoareyou";
    callback(result);
};

exports.roomEvent = function(msg, callback) {
    callback();
};

exports.setInfo = function(params, callback) {
    callback();
};

exports.killMe = function(ip, callback) {
    callback();
};

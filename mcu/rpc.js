var amqp = require('amqp');
var manager = require('./manager');
var config = require('./../../webrtc-mcu/mcu_config');

var TIMEOUT = 3000;

var corrID = 0;
var map = {};   //{corrID: {fn: callback, to: timeout}}
var clientQueue;
var connection;
var exc;

// Create the amqp connection to rabbitMQ server
var addr = {};

if (config.rabbit.url !== undefined) {
    addr.url = config.rabbit.url;
} else {
    addr.host = config.rabbit.host;
    addr.port = config.rabbit.port;
}

exports.connect = function () {

    connection = amqp.createConnection(addr);

    connection.on('ready', function () {
        "use strict";

        console.log('Conected to rabbitMQ server');

        // Create a direct exchange
        exc = connection.exchange(config.rabbit.prefix+'.rpcExchange', {type: 'direct', autoDelete:false}, function (exchange) {
            console.log('Exchange ' + exchange.name + ' is open');

            // Create the queue for receive messages
            var q = connection.queue(config.rabbit.prefix+'.rpcQueue', function (queue) {
                console.log('Queue ' + queue.name + ' is open');

                q.bind(config.rabbit.prefix+'.rpcExchange', 'rpc');

                q.subscribe(function (message) {
                    manager[message.method](message.args, function (result) {
                        exc.publish(message.replyTo, {data: result, corrID: message.corrID});
                    });
                });
            });

            // Create the queue for send messages
            clientQueue = connection.queue('', function (q) {
                console.log('ClientQueue ' + q.name + ' is open');

                clientQueue.bind(config.rabbit.prefix+'.rpcExchange', clientQueue.name);

                clientQueue.subscribe(function (message) {
                    if (map[message.corrID] !== undefined) {
                        map[message.corrID].fn(message.data);
                        clearTimeout(map[message.corrID].to);
                        delete map[message.corrID];
                    }
                });

            });
        });

    });

    connection.on('error', function(e) {
        // ToDo: may not be AMQP error
        console.log('AMQP error: ' + e);
        connected = false;
    });
};

var callbackError = function (corrID) {
    "use strict";

    map[corrID].fn('timeout');
    delete map[corrID];
};

/*
 * Calls remotely the 'method' function defined in manager of 'to'.
 */
exports.callRpc = function (to, method, args, callback) {
    "use strict";

    corrID += 1;
    map[corrID] = {};
    map[corrID].fn = callback;
    map[corrID].to = setTimeout(callbackError, TIMEOUT, corrID);

    var send = {method: method, args: args, corrID: corrID, replyTo: clientQueue.name};

    exc.publish(to, send);

};


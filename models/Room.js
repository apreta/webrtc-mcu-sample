var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var mongoose = require('mongoose');
var randomToken = require('random-token').create('0123456789');

var idLen = 8;

var roomSchema = new mongoose.Schema({
  _id: { type: String, unique: true },
  owner: String,
  name: String,
  description: String,
  tokens: Array
}, { timestamps: true });

// Todo: handle duplicates
roomSchema.pre('save', function (next) {
  var self = this;
  if (this.isNew) {
    self._id = randomToken(idLen);
    next();
  } else {
    next();
  }
});

var Room = mongoose.model('Room', roomSchema);

module.exports = Room;

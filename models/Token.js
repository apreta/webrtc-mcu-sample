var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var mongoose = require('mongoose');
var randomToken = require('random-token').create('0123456789');
var config = require('./../../webrtc-mcu/mcu_config');

var rpcKey = config.rpc.superserviceKey;
var idLen = 8;

var tokenSchema = new mongoose.Schema({
  tokenId: { type: String, unique: true },
  host: String,
  roomId: String,
  guestId: String,
  session: String,
  role: String
}, { timestamps: true });

// Todo: handle duplicates
tokenSchema.pre('save', function (next) {
  var self = this;
  if (this.isNew) {
    self.tokenId = randomToken(idLen);
    next();
  } else {
    next();
  }
});

tokenSchema.methods.calculateSignature = function () {
    var toSign = this.tokenId + ',' + this.host,
        signed = crypto.createHmac('sha1', rpcKey).update(toSign).digest('hex');
    return (new Buffer(signed)).toString('base64');
};

tokenSchema.methods.checkSignature = function () {
    var calculatedSignature = this.calculateSignature();

    if (calculatedSignature !== this.signature) {
        console.log('Auth fail. Invalid signature.');
        return false;
    } else {
        return true;
    }
};

tokenSchema.methods.encode = function () {
    var copy = {
        'tokenId': this.tokenId,
        'host': this.host,
        'signature': this.calculateSignature()
    };
    return new Buffer(JSON.stringify(copy)).toString('base64');
};

tokenSchema.statics.findByToken = function(tokenId,callback) {
    return this.findOne({tokenId: tokenId}, callback);
};

tokenSchema.statics.findByDetails = function(roomId, guestId, sessionId, callback) {
    return this.findOne({roomId: roomId, guestId: guestId, session: sessionId}, callback);
};

var Token = mongoose.model('Token', tokenSchema);

module.exports = Token;

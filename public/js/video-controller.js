(function() {
    "use strict";

    /* Issues/Notes:

       #room-body - container for expanded video 
       #video-list
       #video-bar
       #video-template
       #video-list-template
    
       The video server processes state change messages and keep an up-to-date copy in memory.
       If we are the controller and are reloaded, we'll reload the last known state supplied by the server.
       Currently, only state attributes that have names starting with 'global' are reloaded as these are the core,
       global attributes.

       Mute is now overloaded, need to separate pc Audio from mute so we don't have bo un-muting someone on
       the phone.
       
       ToDo: 
        - combine subscription state into a single object; separate start subscribe vs. subscription complete states.
        - don't subscribe lite clients to sip calls

    */

    var config;
    var templates;
    var i18n;

    var room;
    var localStream;
    var customRate = false;
    var startupTimer = null;
    var cpuTimer = null;

    /* Status flags */
    var connected = false;
    var started = false;
    var stopped = false;
    var reconnect = true;
    var PublishedFlags = { None:0, SentRequest:1, StreamEstablished:2 };
    var published = PublishedFlags.None;
    var masterState = false;
    var expanded = null;
    var mediaSettings = {
        computerAudio: true,    // speaker and mic disabled
        muted: false,           // mic muted
        myVideo: true,
        guestVideo: true,
        boVideo: true,
        phonePause: false,
        guestPause: false
    };

    var maxWidth = 0;
    var videoWidth = 180;
    var maxScroll = 0;

    /* Keep track of who has subscribed to us */
    var subscribed = {};

    /* Track which streams we've paused locally */
    var pausedStreams = {};

    /* Current application state */
    var state = {};

    /* Stream display order */
    var streamDisplay = [];

    var useTimers = true;
    var publishTimer;
    var subscribeTimers = {};
    var reconnectTimers = [];

    var videoDevices = [];
    var audioDevices = [];

    var eventHandlers = {};

    // Keep audio, video timeouts longer then server-side publisher frozen timer 
    var STALLED_TIMEOUT = 30000;
    var LOST_VIDEO_TIMEOUT = 28000;
    var LOST_AUDIO_TIMEOUT = 26000;
    var NO_MEDIA_TIMEOUT = 10000;
    var HIGH_CPU_TIMEOUT = 15000;
    var HIGH_CPU_LIMIT = 0.95;

    var videoConfig = {
        hd: {
            ratio: 9/16,
            invRatio: 16/9,
            bo: { width: 1280, height: 720 },
            guest: { width: 640, height: 360 }
        },
        wide: {
            ratio: 9/16,
            invRatio: 16/9,
            bo: { width: 640, height: 360 },
            guest: { width: 320, height: 180 }
        },
        normal: {
            ratio: 3/4,
            invRatio: 4/3,
            bo: { width: 640, height: 480 },
            guest: { width: 320, height: 240 }
        },
        low: {
            ratio: 3/4,
            invRatio: 4/3,
            bo: { width: 320, height: 240 },
            guest: { width: 320, height: 240 }
        }
    };
    var videoMode = "normal";

    var displayConfig = {
        normal: {
            display: { width: 180, height: 135 }
        },
        small: {
            display: { width: 110, height: 90 }
        }
    };
    var displayMode = "normal";
    
    var bandwidthConfig = {
        initial: { boMaxRate: 720, guestMaxRate: 240 },
        lowCpu1: { boMaxRate: 650, guestMaxRate: 200 },
        lowCpu2: { boMaxRate: 500, guestMaxRate: 100 }
    };
    var bandwidthSetting;

    var monitorStats = {};
        
    /* Reset local state */
    function reset() {
        /* Leave local stream and room alone in case we are reconnecting */
        started = false;
        stopped = false;
        connected = false;
        published = PublishedFlags.None;
        masterState = false;
        expanded = null;
        subscribed = {};
        state = {};
        streamDisplay = [];
        reconnectTimers = [];
        // leave media settings as is
    }

    /* Process state update message */
    function processDelta(msg) {
        if (msg.updates) state = _.extend(state, msg.updates);
        if (msg.removes) state = _.omit(state, msg.removes);

        logger.log("Processing delta:", msg);
        logger.log("New state:", state);

        if (msg.to) {
            // A directed message indicates we've been subscribed to
            if (msg.to == localStream.getID()) {
                if (!subscribed[msg.src]) {
                    subscribed[msg.src] = true;

                    updateState('remote_subscribe', room.remoteStreams[msg.src]);
                }
            }
        } else if (started) {
            // Only forward app-created state changes to app
            video.trigger('stateUpdated', state);
            //saveState();
        }
    }

    function saveState() {
        if (isController() && window.localStorage) {
            localStorage.setItem('video_state', JSON.stringify(state));
            localStorage.setItem('video_time', new Date().getTime());
            localStorage.setItem('video_token', config.token);
        }
    }

    function loadState() {
        if (isController() && window.localStorage) {
            var savedToken = localStorage.getItem('video_token');
            var savedAt = localStorage.getItem('video_time');
            if (savedToken == config.token && (new Date().getTime()) - savedAt < 600000) {
                var lastState = JSON.parse(localStorage.getItem('video_state'));
                setState(lastState);
                return true;
            }
        }
        return false;
    }

    function setState(lastState) {
        var updates = {};
        _.each(lastState, function(v,k) {
            if (k.indexOf('global') === 0) {
                updates[k] = v;
            }
        });
        _.extend(state, updates);
    }

    /* GuestId 0 is the meeting controller */
    function isController(stream) {
        if (stream === undefined)
            return config.guestId == 0;
        else
            return stream.getAttributes().guestId == 0;
    }

    function findController() {
        return _.find(room.remoteStreams, function(s) { return s.getAttributes().guestId == 0; });
    }

    /* Will be undefined for sip call */
    function getGuestId(stream) {
        return stream.getAttributes().guestId;
    }

    /* Send update message */
    function sendUpdates(toStream, type, updates, removes) {
        if (removes === undefined) removes = [];
        var msg = {
            src: localStream.getID(),
            ctl: type,
            updates: updates,
            removes: removes
        };
        if (toStream) msg.to = toStream.getID();
        logger.log("Sending update:", msg);
        localStream.sendData(msg);
        return msg;
    }

    /* Send my state (or all state if we are controller) */
    function sendState(stream) {
        // Don't send state to sip streams.
        var guestId = getGuestId(stream);
        if (guestId == undefined) 
            return;
        if (isController()) {
            logger.log("Sending full state to " + getGuestId(stream));
            sendUpdates(stream, 'initial', state);
        } else {
            logger.log("Sending state for " + config.guestId + " to " + getGuestId(stream));
            var me = config.guestId + ':';
            var updates = {};
            var send = false;
            _.each(state, function(v,k) {
                if (k.lastIndexOf(me, 0) === 0) {
                    updates[k] = v;
                    send = true;
                }
            });
            if (send) {
                sendUpdates(stream, 'delta', updates);
            }
        }
    }

    /* Send request message */
    function sendRequest(type, to) {
        var msg = {
            src: localStream.getID(),
            ctl: type,
            to: to
        }
        logger.log("Sending request:", msg);
        localStream.sendData(msg);
    }

    /* Subscribe to remote stream */
    function subscribeStream(stream) {
        if (stream.getAttributes().sip && !localStream.hasAudio())
            return;
            
        var guestId =  getGuestId(stream);
        logger.log("Subscribing to:", stream.getID(), "-", guestId != undefined ? guestId : 'sip');

        if (!stream.dataListener) {
            stream.addEventListener("stream-data", function (streamEvent) {
                var stream = streamEvent.stream;
                var msg = streamEvent.msg;
                logger.log("Received from " + getGuestId(stream) + ":", msg);
                switch (msg.ctl) {
                    case 'refresh':
                        if (!msg.to || msg.to == localStream.getID()) {
                            sendState(stream);
                        }
                        break;
                    case 'initial':
                    case 'delta':
                        processDelta(msg);
                        break;
                }
            });
            stream.dataListener = true;
        }

        room.subscribe(stream);
        if (stream.getAttributes().sip) {
            subscribed[stream.getID()] = true;
        }
        if (useTimers) {
            // Set up retry timer
            clearTimeout(subscribeTimers[stream.getID()]);
            subscribeTimers[stream.getID()] = setTimeout(function () {
                logger.log("Subscribe timeout: "+stream.getID());
                restartVideo(stream.getID());
            }, STALLED_TIMEOUT);
        }
    }

    function unsubscribeStream(stream) {
        removeVideo(stream);
        room.unsubscribe(stream);
        clearTimeout(subscribeTimers[stream.getID()]);
    }

    /* Subscribe to list of streams (i.e. current room guests) */
    function subscribeToStreams(streams) {
        for (var index in streams) {
            var stream = streams[index];
            if (localStream.getID() !== stream.getID()) {
                subscribeStream(stream);
            }
        }
    }

    /* Publish my stream to room */
    function publishStream() {
        if (!published) {
            localStream.getAttributes().hasCamera = config.video && videoDevices.length > 0;
            logger.log("Publishing local stream:", localStream.getAttributes());
            var br = isController() ? 512 : 256;
            room.publish(localStream, customRate ? {} : {maxVideoBW: br});
            published = PublishedFlags.SentRequest;

            // Set up retry timer
            if (useTimers) {
                clearTimeout(publishTimer);
                publishTimer = setTimeout(function() {
                    if (published != PublishedFlags.StreamEstablished) {
                        logger.log("Publish timeout");
                        restartVideo(localStream.getID());
                    }
                }, STALLED_TIMEOUT);
            }
        }
    }

    function unpublishStream() {
        if (published) {
            logger.log("Unpublishing local stream");
            localStream.priorId = localStream.getID();
            removeVideo(localStream);
            room.unpublish(localStream);
            // room.unpublish seems to clear the stream id so we can't recognize our stream in the event handler so
            // clear published flag now
            published = PublishedFlags.None;
            clearTimeout(publishTimer);
        }
    }
    
    function startEvent() {
        if (!started) {
            started = true;
            updateState('started');
            //loadState();
            video.trigger('started', makeParticipantList(), state);
        }
    }

    /*
    function findVideo(stream) {
        var element = document.getElementById(stream.elementID);
        return element;
    }

    function findVideoForGuest(id) {
        var element = $(".guest-"+id);
        return element.length ? element[0] : null;
    }
    */

    /* Create video player and show stream */
    function showVideo(stream, local) {
        var parent = $('#video-list');
        var sid = stream.getID();
        var attribs = stream.getAttributes();
        var avatar = attribs.avatar;
        if (attribs.sip) {
            parent.prepend(templates.videoTemplate({id: sid, guestId: -1, type: "phone",
                avatar: '', name: '', controller: false, hasCamera: false}));
        } else {
            var content = templates.videoTemplate({id: sid, guestId: attribs.guestId, type: (local ? "local" : "remote"),
                    avatar: avatar, name: attribs.displayName, controller: isController(), hasCamera: attribs.hasCamera});
            // See if we should replace an existing video container (e.g. stream is reconnecting)
            var curr = parent.find('.video-container.guest-'+attribs.guestId);
            if (curr.length) {
                curr.replaceWith(content);
            } else if (local && displayMode == 'small') {
                // If small layout put self video outside scrolling box
                $("#video-self").prepend(content);
            } else if (isController(stream)) {
                curr = parent.find(".video-container.guest-"+config.guestId);
                if (curr.length) {
                    curr.after(content);
                } else {
                    parent.prepend(content);
                }
            } else {
                parent.append(content);
            }
        }
        if (attribs.guestId != undefined && reconnectTimers[attribs.guestId]) {
            clearTimeout(reconnectTimers[attribs.guestId]);
            delete reconnectTimers[attribs.guestId];
        }

        updateVideoControls(sid);
        
        $("#video_mute_"+sid).click(function(e) {
            var set = $(this).is('.enabled');
            var settings = getStreamAttribute(sid, "mediaSettings");
            if (settings.computerAudio)
                updateAttributes(sid, { mediaSettings: { muted: !set } });
        });

        $("#video_expand_"+sid).click(function(e) {
            if (!$(this).hasClass('disabled'))
                video.trigger('expandClick', sid);
        });

        $("#video_pause_"+sid).click(function(e) {
            if (!$(this).hasClass('disabled')) {
                var enable = !$(this).is('.enabled');
                pauseVideo(sid, enable);
            }
        });

        stream.show('vcont_'+sid, local ? {muted: true, media: config.media, className: 'video_player local', bar: false} :
            {media: config.media, className:'video_player', bar: false});
        streamDisplay = _.union(streamDisplay, sid);

        stream.volumeElem = $("#volume_" + sid);
        $("#player_" + sid).css('z-index', '100');
        
        resizeVideo(sid, true);
        resizeVideoList();

        // If user has no camera we substitute their avatar (probably initials)
        if (!attribs.hasCamera || !config.media) {
            showStaticImage(sid, true);
        }

        if (local && !mediaSettings.myVideo)
            myVideo(false);

        setTimeout(function() {
            if (!mediaSettings.guestVideo && !isController(stream) && !local)
                pauseVideo(sid, true);
            if (!mediaSettings.boVideo && isController(stream))
                pauseVideo(sid, true);
            if (!mediaSettings.computerAudio || (local && mediaSettings.muted))
                muteAudio(sid, true);
            if (expanded && expanded.id == sid)
                toggleVideo(expanded.id, true, expanded.container, expanded.forced);
        }, 2000);
    }

    /* Stream's hasCamera flag has changed */
    function changeVideo(stream) {
        if (stream.getAttributes().hasCamera) {
            $('#vcont_' + stream.getID()).removeClass('nowebcam').addClass('webcam');
            showStaticImage(stream.getID(), false);
        } else {
            $('#vcont_' + stream.getID()).removeClass('webcam').addClass('nowebcam');
            // If user has no camera we substitute their avatar (probably initials)
            showStaticImage(stream.getID(), true);
        }
    }

    /* Set width of solid color div to create volume level indicator */
    function showVolume(stream, vol) {
        var mediaSettings = stream.getAttributes().mediaSettings;
        if (mediaSettings.muted || mediaSettings.computerAudio == false) vol = 0;
        if (started)
            video.trigger('audioLevel', stream.getID(), vol);
        vol = vol/32767 * videoWidth;
        $(stream.volumeElem).width(Math.floor(vol));
    }

    /* Remove video element */
    function removeVideo(stream) {
        logger.log("Removing video: " + stream.getID());
        
        // Collapse if expanded
        if (expanded && expanded.id == stream.getID()) {
            toggleVideo(expanded.id, false, expanded.container);
        }
        
        // Stop watching stats
        monitorStop(stream);

        // If not stopping all, leave video container on screen for a bit in case remote stream reconnects
        try {
            streamDisplay = _.filter(streamDisplay, function(id) { return id != stream.getID(); });
            if (!stopped) {
                $("#vcont_"+stream.getID()).find(".video-reconnect").show();
                if (stream.getID() != localStream.getID()) {
                    var elemId = stream.elementID;
                    var guestId = stream.getAttributes().guestId;
                    if (guestId != undefined) {
                        reconnectTimers[guestId] = setTimeout(function() {
                            logger.log("Timed out video:" + elemId);
                            $("#"+elemId).remove();
                            resizeVideoList();
                        }, 30000);
                    }
                }
            } else {
                // Remove stream from DOM
                if (stream.elementID !== undefined) {
                    var element = document.getElementById(stream.elementID);
                    var parent = document.getElementById('video-list');
                    parent.removeChild(element);
                    resizeVideoList();
                }
            }
        } catch (error) {
            logger.log("Remove video error: " + error);
        }
    }

    /* Update video control icons based on current state */
    function updateVideoControls(id) {
        var stream;
        var settings;
        var status = [];
        if (id == undefined || id == localStream.getID()) {
            id = localStream.getID();
            stream = localStream;
            settings = mediaSettings;
        }
        else {
            stream = getStream(id);
            if (!stream) return;
            settings = stream.getAttributes().mediaSettings;
            if (!settings) return;
        }
        if (!stream.hasAudio() && !stream.hasVideo()) {
            $("#video_mute_"+id).addClass('enabled').addClass('disabled');
            status.push('Video Lite');
        } else if (!settings.computerAudio) {
            $("#video_mute_"+id).addClass('enabled').addClass('disabled');
            status.push('Audio Off');
        } else if (settings.muted) {
            $("#video_mute_"+id).addClass('enabled').removeClass('disabled');
            status.push('Muted');
        } else {
            $("#video_mute_"+id).removeClass('enabled').removeClass('disabled');
        }
        if (expanded && expanded.id == id) {
            $("#video_expand_"+id).addClass("enabled");
        } else {
            $("#video_expand_"+id).removeClass("enabled");
        }
        if (expanded && expanded.id == id && expanded.forced) {
            $("#video_expand_"+id).addClass("disabled");
        } else {
            $("#video_expand_"+id).removeClass("disabled");
        }
        if (!stream.hasVideo() || id == localStream.getID()) {
            $("#video_pause_"+id).removeClass('enabled').addClass('disabled');
        } else if (pausedStreams[id]) {
            $("#video_pause_"+id).addClass('enabled').removeClass('disabled');
        } else {
            $("#video_pause_"+id).removeClass('enabled').removeClass('disabled');
        }
        if (!stream.hasVideo() || !config.media) {
            //$("#video_expand_"+id).addClass("disabled");
        } else if (!settings.myVideo) {
            status.push("Video Off");
        } else if (pausedStreams[id]) {
            status.push('Video Paused');
        }

        status = status.join(", ");
        if (status.length)
            $("#video_status_"+id).text(status).addClass('enabled');
        else
            $("#video_status_"+id).removeClass('enabled');

    }

    /* Show/hide static image in place or instead of video */
    function showStaticImage(id, flag) {
        var $cont = $("#vcont_"+id);
        var $static = $cont.find('div.video-static');
        if (flag) {
            $static.show();
            $cont.addClass('expanded');
        } else {
            $static.hide();
            $cont.removeClass('expanded');
        }
    }

    function resizeVideo(id, crop) {
        if (crop == undefined) crop = false;
        if (expanded && expanded.id == id)
            var $cont = $("#player_"+id);
        else
            var $cont = $("#vcont_"+id);
        var $vid = $cont.find('video');

        var width = $cont.width(),
            height = $cont.height();

        var ratio = videoConfig[videoMode].ratio;
        if (expanded && expanded.id == id && $vid.prop('videoWidth')) {
            ratio = $vid.prop('videoHeight') / $vid.prop('videoWidth');
        }
        var invRatio = 1 / ratio;
        
        if (!crop) {
            if (height * (invRatio) > width) {
                $vid.css('width', width + "px")
                    .css('height', (ratio) * width + "px")
                    .css('top', -((ratio) * width / 2 - height / 2) + "px")
                    .css('left', "0px");
            } else {
                $vid.css('height', height + "px")
                    .css('width', (invRatio) * height + "px")
                    .css('left', -((invRatio) * height / 2 - width / 2) + "px")
                    .css('top', "0px");
            }
        } else {
            var contRatio = height / width;
            if (contRatio < ratio) {
                $vid.css('width', width + "px")
                    .css('height', (ratio) * width + "px")
                    .css('top', -((ratio) * width / 2 - height / 2) + "px")
                    .css('left', "0px");
            } else {
                $vid.css('height', height + "px")
                    .css('width', (invRatio) * height + "px")
                    .css('left', -((invRatio) * height / 2 - width / 2) + "px")
                    .css('top', "0px");
            }
        }
    }

    function getVideoPosition(id) {
        var $video = $("#player_" + id).find('video');
        var ratio = videoConfig[videoMode].ratio;
        if ($video.prop('videoWidth')) {
            ratio = $video.prop('videoHeight') / $video.prop('videoWidth');
        }
        if (expanded && expanded.id == id && !expanded.media) {
            $video = $(expanded.container).find('div.video-static.copy');
            ratio = 1;
        }
        var width = $video.width(),
            height = $video.height(),
            pos = $video.offset(),
            vpos = null;

        if (pos) {
            if (videoMode != "normal") {
                if (width * ratio < height) {
                    vpos = { width: width, height: ratio * width,
                            top: pos.top + (height / 2 - ratio * width / 2) - $video[0].offsetTop, 
                            left: pos.left + $video[0].offsetLeft 
                    };
                } else {
                    vpos = { height: height, width: 1/ratio * height,
                            left: pos.left + (width / 2 - 1/ratio * height / 2) + $video[0].offsetLeft, 
                            top: pos.top - $video[0].offsetTop
                    };
                }
            } else {
                vpos = { width: width, height: height,
                        top: pos.top /* + (height / 2 - ratio * width / 2) - $video[0].offsetTop */, 
                        left: pos.left /* + $video[0].offsetLeft */ 
                };
            }
        }
        return vpos;
    }

    function resizeVideoList(width) {
        if (width !== undefined) {
            if (displayMode == 'small') {
                width -= (videoWidth + 5); // leave room for self video
            }
            maxWidth = width;
            $("#video-bar").width(width);
        }
        else {
            width = maxWidth;
        }

        var larrow = $("#video-left");
        var rarrow = $("#video-right");

        var listWidth = width - larrow.width() - rarrow.width();

        var parent = $('#video-list');
        var videos = parent.find(".video-container");

        var maxVideos = Math.floor(listWidth/videoWidth);
        parent.width(Math.min(maxVideos,videos.length) * videoWidth);
        maxScroll = Math.max(videoWidth*(videos.length-maxVideos),0);
        
        $(".spacer-arrow").width((width - parent.width()) / 2 - larrow.width());

        scrollList(0);
    }

    function scrollList(offset) {
        var list = $("#video-list");
        var pos = parseInt(list.css('left'));
        if (isNaN(pos)) pos = 0;
        pos += offset;
        if (pos > 0) { pos = 0; }
        if (-pos > maxScroll) { pos = -maxScroll; }

        function checkHidden() {
            var start = -pos, end = start + list.width();
            list.find(".video-container").each(function() {
                var hide = (this.offsetLeft < start || this.offsetLeft >= end);
                $(this).css("opacity", hide ? "0" : "1");
            });
        }
        
        list.animate({'left': pos+'px'}, {
            complete: function() {
                checkHidden();
            }
        });
    
        if (pos == 0) {
            $("#video-left").addClass('disabled');
        } else {
            $("#video-left").removeClass('disabled');
        }

        if (pos == -maxScroll) {
            $("#video-right").addClass('disabled');
        } else {
            $("#video-right").removeClass('disabled');
        }
    }

    /* Make guest descriptor for a stream */
    function makeParticipant(stream) {
        var idx = _.indexOf(streamDisplay, stream.getID());
        return _.extend(stream.getAttributes(), {id: stream.getID(), displayIndex: idx >= 0 ? idx : 99 });
    }

    /* Make list of current participants */
    function makeParticipantList() {
        var list = _.filter(room.remoteStreams, function(s) { return !s.getAttributes().sip; });
        return _.map(list, function(s) { return makeParticipant(s); });
    }

    function getStream(streamId) {
        var stream = room.remoteStreams[streamId];
        if (stream == undefined) stream = room.localStreams[streamId];
        if (stream == undefined) stream = null;
        return stream;
    }

    function getStreamAttribute(streamId, field) {
        var stream = room.remoteStreams[streamId];
        if (stream == undefined) stream = room.localStreams[streamId];
        if (stream == undefined) return null;
        return stream.getAttributes()[field];
    }

    /* Build list of source devices (cameras, microphones, etc.) */
    /* N.B.: if called before webcam access is granted we won't get device names but should find out
      if user has a camera. */
    function refreshSources(callback) {
        videoDevices = [];
        audioDevices = [];

        if (window.MediaStreamTrack && MediaStreamTrack.getSources) {
            MediaStreamTrack.getSources(function(sourceInfos) {
                var audio_count = 0;
                var video_count = 0;
                for (var i = 0; i < sourceInfos.length; i++) {
                    var source = {
                        id: sourceInfos[i].id,
                        label: sourceInfos[i].label,
                        type: sourceInfos[i].kind
                    };
                    if (sourceInfos[i].kind === 'audio') {
                        audio_count++;
                        if (source.label === '') {
                            source.label = 'Audio ' + audio_count;
                        }
                        audioDevices.push(source);
                    } else {
                        video_count++;
                        if (source.label === '') {
                            source.label = 'Video ' + video_count;
                        }
                        videoDevices.push(source);
                    }
                }

                if (callback) callback();
            });
        } else {
            /*videoDevices.push({kind:'video',id:1});
            audioDevices.push({kind:'audio',id:2});*/
            if (callback) _.delay(callback);
        }
    }

    function getDeviceIds(audioId, videoId) {
        var devices = {};
        if (audioDevices.length > audioId) {
            devices.audioId = audioDevices[audioId].id;
        }
        if (videoDevices.length > videoId) {
            devices.videoId = videoDevices[videoId].id;
        }
        return devices;
    }
    
    /*
     * Periodically check CPU load.
     */
    function monitorCPU() {
        var checkTimeout = 500;
        var highCountMax = HIGH_CPU_TIMEOUT/checkTimeout;
        var count = 0;
        var numCPUs = 0;
        var highCount = 0;
        var totalHighCount = 0;
        var reports = 0;
        var isBO = isController();

        if (cpuTimer) {
            clearTimeout(cpuTimer);
            cpuTimer = null;
        }

        var warnUser = function() {
            logger.log('Warning user about CPU');
            var msg = {
                height: 220,
                title: i18n._("Your computer is running slow"),
                text: i18n._("We have detected high CPU usage on your computer for more then 15 seconds.  This can result in " +
                    "audio problems such as echo and drop outs.<br><br>" + 
                    "Please close all other browser tabs, windows, and applications to reduce your CPU usage.")
            };
            video.trigger("showMessage", msg);
        };

        var pauseMyVideo = function() {
            logger.log('** Pausing my video');
            var msg = {
                height: 180,
                title: i18n._("Your computer is running slow"),
                text: i18n._("To help improve your video experience we have paused your outgoing video stream.<br>" +
                      "Other guests will see your snapshot but will hear you normally.")
            };
            if (mediaSettings.myVideo) {
                myVideo(false);
                video.trigger("showMessage", msg);
                video.updateAttributes(localStream.getID(), { mediaSettings: mediaSettings });
            }
        };

        var pauseVideos = function(reason) {
            logger.log('** Pausing guest videos: ' + reason);
            var msg = {
                height: 200,
                title: i18n._("Your computer is running slow"),
                text: i18n._("For the best video experience, we paused all but the leader's video stream.<br><br>" +
                      "To play all videos, click the play guest videos button above (<i class=\"fa fa-play\"></i>).")
            };
            if (mediaSettings.guestVideo) {
                guestVideo(false);
                video.trigger("showMessage", msg);
                video.updateAttributes(localStream.getID(), { mediaSettings: mediaSettings });
            }
        };

        var highCPU = function() {
            ++totalHighCount;
            ++highCount;
            if (highCount > highCountMax) {
                ++reports;
                if (context.isMobileApp) {
                    logger.log("** High CPU load +");
                    setMaxBandwidth(bandwidthConfig.lowCpu1);
                    pauseVideos('max');
                    highCount = 0;
                } else {
                    if (reports == 1) {
                        logger.log("** High CPU load 1");
                        warnUser();
                        highCount = 0;
                    } else if (reports == 2) {
                        logger.log("** High CPU load 2");
                        setMaxBandwidth(bandwidthConfig.lowCpu1);
                        pauseVideos('max');
                        highCount = 0;
                    } else if (reports == 3) {
                        logger.log("** High CPU load 3");
                        setMaxBandwidth(bandwidthConfig.lowCpu2);
                        if (!isBO)
                            pauseMyVideo();
                        highCount = 0;
                    } else {
                        logger.log("** High CPU load +");
                        // Try again in case user turned them back on
                        pauseVideos('max');
                        highCount = 0;
                    }
                }
            }
        };

        var cpu = function(info) {
            if (numCPUs == 0 && info.number > 0) {
                numCPUs = info.number;
                logger.log("Number CPUs: " + numCPUs);
                monitorStats.numCPUs = numCPUs;
                if (mediaSettings.guestVideo && numCPUs < 2)
                    pauseVideos('num_cpus');
            }
            if (numCPUs > 0) {
                var load = info.usage / numCPUs;
                if (load > HIGH_CPU_LIMIT) {
                    highCPU();
                } else {
                    highCount = 0;
                }
                if (++count % 10 == 0)
                    logger.log("CPU load: " + load);
            }
        };

        var check = function() {
            var info = {number: window.app.getNumCPUs(), usage: window.app.getCPULoad() };
            cpu(info);
            cpuTimer = setTimeout(check, checkTimeout);
        };
        
        var checkViewer = function() {
            parent.postMessage({command:'cpu'}, '*');
        };

        window.addEventListener('message', function(event) {
            if (event.data.id == 'cpu') {
                cpu(event.data);
                cpuTimer = setTimeout(checkViewer, checkTimeout);
            }
        });
        
        var checkExtension = function() {
            if (window.chrome) {
                chrome.runtime.sendMessage(context.cpuExtensionId, {command: 'cpu'}, function(info) {
                    cpu(info);
                    cpuTimer = setTimeout(checkExtension, checkTimeout);
                });
            }
        }

        if (window.app && window.app.getNumCPUs) {
            cpuTimer = setTimeout(check, checkTimeout);
        } else if (context.isMobileApp) { // cordova-based viewer
            //cpuTimer = setTimeout(checkViewer, checkTimeout);
        } else if (window.chrome && chrome.runtime) {
            cpuTimer = setTimeout(checkExtension, checkTimeout);
        }
    }

    /*
     * Periodically capture peer connection stats.
     * We can use the bytesSent to detect if we've stopped sending video, and audioInputLevel to see if the
     * microphone is working.
     */
    function monitorStart(stream, local) {
        var checkTimeout = context.isMobileApp ? 500 : 250; 
        var streamId = stream.getID();
        var attribs = stream.getAttributes();
        var thisStats =  {
            started: Date.now(),
            samples: 0,
            bytes: 0,
            stalledBytes: 0,
            lastChange: Date.now(),
            id: 0,
            audioLevel: 0,
            soundReport: false,
            videoReport: false,
            frameWidth: attribs.hasCamera ? 0 : 1
        };
        monitorStats[streamId] = thisStats;

        var log = function(res) {
            var names = res.names();
            var label = "[" + streamId + "] " + res.type + ": ";
            var str = _.reduce(names, function(memo, name) { return memo + name + ": " + res.stat(name) + ", "; }, label);
            logger.log(str);
        };

        var warnUser = function(media, device) {
            logger.log('Warning user about no ' + media);
            if (context.isViewer) {
                var text = "We are not receiving %s from your %s. Restarting the viewer by closing this window may fix the problem.";
            } else if (context.isMac) {
                var text = "We are not receiving %s from your %s. Restarting your browser may fix the problem.  " +
                    "Before restarting, you must completely close Google Chrome by control clicking the Chrome dock icon and selecting 'Quit', then re-join the room.";
            } else {
                var text = "We are not receiving %s from your %s. Restarting your browser may fix the problem.  " +
                    "Before restarting, close all Google Chrome windows including this one, then re-join the room.";
            }
            text += "<br><br>If this doesn't help, click <a href='#' onclick='video.trigger(\"showSettings\");'>Audio/Video Problems?</a> and follow the instructions.";
            var msg = {
                height: 250,
                title: i18n.translate("No %s input detected").fetch(media),
                text: i18n.translate(text).fetch(media, device)
            };
            video.trigger("showMessage", msg);
        };
        
        var process = function(stats) {
            if (!monitorStats[streamId]) return;
            var sendLog = (++thisStats.samples % 20) == 0;

            var results = stats.result();
            for (var i = 0; i < results.length; ++i) {
                var res = results[i];
                // "googCandidatePair" might be interesting too
                if (sendLog && (res.type == 'ssrc' || res.type == 'VideoBwe'))
                    log(res);

                if (local && (!res.local || res.local === res)) {
                    // The bandwidth info for video is in a type ssrc stats record
                    // with googFrameHeightXXX defined.
                    if (res.type == 'ssrc') {
                        if (res.stat('googFrameWidthInput')) {
                            thisStats.frameWidth = res.stat('googFrameWidthInput');
                            if (thisStats.frameWidth > 0) thisStats.videoReport = true;
                            if (!thisStats.videoReport && mediaSettings.myVideo && Date.now() - thisStats.started > NO_MEDIA_TIMEOUT) {
                                thisStats.videoReport = true;
                                warnUser("video", "webcam");
                            }
                            // "bytesSent", "googNacksReceived"
                            var bytes = res.stat('bytesSent');
                            if (bytes != thisStats.bytes) {
                                thisStats.lastChange = Date.now();
                                thisStats.bytes = bytes;
                            } else if (bytes != thisStats.stalledBytes) {
                                if (Date.now() - thisStats.lastChange > LOST_VIDEO_TIMEOUT) {
                                    thisStats.stalledBytes = bytes;
                                    logger.log('Local video not being sent, restarting');
                                    restartVideo();
                                    return;
                                }
                            } // else stopped and already reported
                        } else if (res.stat('audioInputLevel')) {
                            // "audioInputLevel", "googJitterReceived"
                            thisStats.audioLevel = res.stat('audioInputLevel');
                            showVolume(stream, res.stat('audioInputLevel'));
                            if (thisStats.audioLevel > 0) thisStats.soundReport = true;
                            if (!thisStats.soundReport && !mediaSettings.muted && Date.now() - thisStats.started > NO_MEDIA_TIMEOUT) {
                                thisStats.soundReport = true;
                                warnUser("audio", "mic");
                            }
                        }
                    }
                } else if (!local && (!res.remote || res.remote === res)) {
                    if (res.type == 'ssrc') {
                        if (res.stat('googFrameWidthReceived')) {
                            // Input video stream
                            thisStats.frameWidth = res.stat('googFrameWidthReceived');
                        } else if (res.stat('audioOutputLevel')) {
                            // Input audio stream
                            thisStats.audioLevel = res.stat('audioOutputLevel');
                            if (!attribs.sip) showVolume(stream, res.stat('audioOutputLevel'));
                            if (thisStats.audioLevel > 0) thisStats.soundReport = true;
                            var bytes = res.stat('bytesReceived');
                            if (bytes != thisStats.bytes) {
                                thisStats.lastChange = Date.now();
                                thisStats.bytes = bytes;
                            } else if (bytes != thisStats.stalledBytes) {
                                if (Date.now() - thisStats.lastChange > LOST_AUDIO_TIMEOUT) {
                                    thisStats.stalledBytes = bytes;
                                    /*if (mediaSettings.computerAudio) {*/
                                        logger.log("Remote audio not being received, restarting:", streamId);
                                        restartVideo(streamId);
                                        return;
                                    /*}*/
                                }
                            } // else stopped and already reported
                        }
                    }
                }
            }

            thisStats.id = setTimeout(function() {
                if (stream.pc && stream.pc.peerConnection) {
                    var pc = stream.pc.peerConnection;
                    pc.getStats(process);
                } else {
                    logger.log("Peer connection went missing");
                }
            }, checkTimeout);
        };

        if ((config.video || config.audio) && stream.pc && stream.pc.peerConnection.getStats && !context.isMobileApp)
            stream.pc.peerConnection.getStats(process);
    }

    function monitorAudio(stream) {
        // Only works for local stream, see chrome issue 121673
        var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
        var source = audioCtx.createMediaStreamSource(stream.stream);
        var analyser = audioCtx.createAnalyser();
        source.connect(analyser);
        analyser.fftSize = 32;   // minimum, yields 16 frequency bins
        analyser.smoothingTimeConstant = 0;
        var dataArray = new Uint8Array(analyser.frequencyBinCount);

        var audioLevel = function() {
            analyser.getByteFrequencyData(dataArray);
            var out = "";
            for (var i = 0; i < dataArray.length; i++) out += dataArray[i] + " ";
            //console.log("Level: ", streamId, out);
        };

        setInterval(audioLevel, 500);
    }

    function monitorStop(stream) {
        var streamId = stream ? stream.getID() : localStream.getID();

        if (monitorStats && monitorStats[streamId]) {
            clearTimeout(monitorStats[streamId].id);
            delete monitorStats[streamId];
        }
    }

    function updateState(event, stream) {
        logger.log("** Event: " + event + (stream ? ", stream: " + stream.getID() : ""));
        var delayed = false;

        clearTimeout(startupTimer);
        startupTimer = setTimeout(function() {
            if (!started) {
                logger.log("Delay during startup, skipping to next step");
                updateState('delayed');
            }
        }, STALLED_TIMEOUT);

        switch (event) {
            case 'connecting':
                stopped = false;
                break;
            case 'connected':
                if (isController())
                    masterState = true;
                break;
            case 'published':
                clearTimeout(publishTimer);
                // Send my state to other guests
                _.each(room.remoteStreams, function(s) {
                    if (s.getID() != localStream.getID())
                        sendState(s);
                });
                break;
            case 'remote_published':
                // Send my/all state to new guest
                if (published) {
                    sendState(stream);
                }
                break;
            case 'subscribe':
                clearTimeout(subscribeTimers[stream.getID()]);
                break;
            case 'remote_subscribe':
                if (isController(stream)) {
                    masterState = true;
                }
                break;
            case 'stopped':
                stopped = true;
                clearTimeout(startupTimer);
                clearTimeout(publishTimer);
                _.each(subscribeTimers, function(t) {
                    clearTimeout(t);
                });
                break;
            case 'started':
                clearTimeout(startupTimer);
                break;
            case 'delayed':
                delayed = true;
                break;
        }

        if (stopped) {
            logger.log("Video stopped");
        }
        else if (!connected) {
            if (delayed) {
                logger.log("Unable to connect to room, trying again");
                stop(true);
            }
        }
        else if (published != PublishedFlags.StreamEstablished) {
            if (published != PublishedFlags.SentRequest) {
                publishStream();
            }
            // ToDo: if delayed, consider offering fallback to lite
        }
        else if (!started) {
            // Note: we won't get here until our stream is published.  This shouldn't really be necessary,
            // but we don't show up in the participant list until that point.

            if (delayed || masterState || !findController()) {
                // We've received state from controller, or there is no controller, so start
                logger.log("All systems go");
                startEvent();
            }
        }
    }

    /* Restart video stream */
    function restartVideo(streamId) {
        logger.log("Restarting video:", streamId);
        if (streamId == undefined || streamId == localStream.getID()) {
            // ToDo: restart video without un-publishing
            unpublishStream();
            setTimeout(function() {
                if (published == PublishedFlags.None)
                    publishStream();
            }, 1000);
        } else {
            var stream = room.remoteStreams[streamId];
            if (stream) {
                unsubscribeStream(stream);
                setTimeout(function() {
                    // Make sure room & stream is still around
                    if (room && room.remoteStreams[streamId])
                        subscribeStream(stream);
                }, 2000);
            } else {
                logger.log("Remote stream not found");
            }
        }
    }

    /* Replacement logger */
    function log(level) {
        var out = '';
        if (level < L.Logger.logLevel) {
            return;
        }
        if (level === L.Logger.DEBUG) {
            out = out + "DEBUG";
        } else if (level === L.Logger.TRACE) {
            out = out + "TRACE";
        } else if (level === L.Logger.INFO) {
            out = out + "INFO";
        } else if (level === L.Logger.WARNING) {
            out = out + "WARNING";
        } else if (level === L.Logger.ERROR) {
            out = out + "ERROR";
        }
        out = out + ": ";
        var args = [];
        for (var i = 0; i < arguments.length; i++) {
            args[i] = arguments[i];
        }
        args[0] = out;
        logger.log(args.join(' '));
    };

    /*
     * Implement various media settings
     */

    function computerAudio(enabled) {
        mediaSettings.computerAudio = enabled;
        muteAudio('all', !enabled);
    }

    function myVideo(enabled) {
        mediaSettings.myVideo = enabled;
        pauseVideo(null, !enabled);
        updateAttributes(localStream.getID(), { hasCamera: enabled ? videoDevices.length > 0 : false });
    }

    function guestVideo(enabled) {
        mediaSettings.guestVideo = enabled;
        pauseVideo('guests', !enabled);
    }

    function boVideo(enabled) {
        mediaSettings.boVideo = enabled;
        _.each(room.remoteStreams, function(s) {
            if (s.getID() != localStream.getID() && isController(s))
                pauseVideo(s.getID(), !enabled);
        });
    }

    function phonePause(enabled) {
        mediaSettings.phonePause = enabled;
        computerAudio(!enabled);
        guestVideo(!enabled);
    }

    function guestPause(enabled) {
        mediaSettings.guestPause = enabled;
        guestVdeo(!enabled);
    }

    var changeSetting = {
        computerAudio: computerAudio,
        myVideo: myVideo,
        guestVideo: guestVideo,
        boVideo: boVideo,
        phonePause: phonePause,
        guestPause: guestPause
    };

    function saveMediaSettings() {
        if (window.localStorage) {
            localStorage.setItem('video_settings', JSON.stringify(mediaSettings));
            localStorage.setItem('video_token', config.token);
            localStorage.setItem('video_time', Date.now());
        }
    }

    function loadMediaSettings() {
        if (window.localStorage) {
            if (localStorage.getItem('video_token') == config.token) {
                if (localStorage.getItem('video_settings')) {
                    try {
                        mediaSettings = JSON.parse(localStorage.getItem('video_settings'));
                    } catch (e) {
                        logger.log('Error parsing stored media settings:', e);
                    }
                }
            }
        }
        if (config.options) {
            // Option to auto-start guests as paused...
            if (config.options.pauseGuestsAtStart) {
                mediaSettings.guestPause = true;
                mediaSettings.guestVideo = false;
            }
        }
    }

    function setMaxBandwidth(setting) {
        if (setting == undefined) {
            setting = bandwidthConfig.initial;
        }
        bandwidthSetting = setting;
        var rateCap = isController() ? setting.boMaxRate : setting.guestMaxRate;

        if (rateCap && window.app && window.app.setMaxBitRate) {
            customRate = true;
            logger.log("Set bit rate cap to " + rateCap);
            app.setMaxBitRate(rateCap);
        }
        // If not using viewer with setMaxBitRate API we can't dynamically adjust rate.
        // We could restart our stream, but adjusting the bitrate in that way is unlikely
        // to effect the CPU usage (the custom setMaxBitRate works by dropping frames 
        // which does reduce CPU load, but there's no way to achieve the same thing in
        // an unmodified browser).
    }

    /*
     * External APIs
     */

    /**
     * Initialize video controller.
     */
    function init(_config) {
        L.Logger.log = log;
        
        if (_config !== undefined) {
            config = _config;
            i18n = _config.i18n;
            if (config.bandwidthConfig != undefined)
                _.extend(bandwidthConfig, config.bandwidthConfig);
            if (config.options.highCPULimit != undefined) HIGH_CPU_LIMIT = config.options.highCPULimit;
            if (config.options.highCPUTime != undefined) HIGH_CPU_TIMEOUT = config.options.highCPUTime;
            if (config.options.videoMode != undefined) videoMode = config.options.videoMode;
            if (config.size != undefined) displayMode = config.size;
            // Allow inbound media even if webcam/mic not present
            config.media = (config.media && navigator.webkitGetUserMedia !== undefined);
            videoWidth = displayConfig[displayMode].display.width;
        }
        reset();
        logger.log("Token:" + config.token);
        templates = {
            videoTemplate: _.template($("#video-template").html()),
            listTemplate: _.template($("#video-list-template").html())
        };

        $('#'+config.videoContainerId).empty().append(templates.listTemplate({}));

        $("#close-video-button").unbind().click(function() {
            if (expanded.id)
                video.trigger('expandClick', expanded.id);
                if (talkers) {
                    talkers.off();
                }
        });

        $('#video-left').click(_.partial(scrollList, videoWidth));
        $('#video-right').click(_.partial(scrollList, -videoWidth));

        $("#video-bar").swipe({
            swipeLeft: function(event, direction, distance, duration, fingerCount) {
                scrollList(-videoWidth);
            }, 
            swipeRight: function(event, direction, distance, duration, fingerCount) {
                scrollList(videoWidth);
            }
        });
        
        if (config.media) {
            refreshSources(function() {
                // If no camera try to continue with just mic.
                config.video = config.video && videoDevices.length > 0;
                //config.audio = config.audio && audioDevices.length > 0;
                if (videoDevices.length == 0) { logger.log('No webcam found'); }
                if (audioDevices.length == 0) { logger.log('No audio found'); }
                _.delay(function() { video.trigger('initialized'); });
            });
        } else {
            logger.log("Ignoring any devices");
            videoDevices = [];
            audioDevices = [];
            _.delay(function() { video.trigger('initialized'); });
        }
    }

    /**
     *  Start streaming and event processing.
     */
    function start() {
        // Select higher res stream for presenter as they will be full screen often
        // Other options: 960x720 (HD), 1280x720 (HD wide), 640x360 (wide), 320x180 (wide)
        var size = isController() ? videoConfig[videoMode].bo : videoConfig[videoMode].guest;
        size.minWidth = size.width;
        size.minHeight = size.height;
        if (config.size == 'small') {
            // Android phones seem to have more limited support so give it a range
            // (it's hard to detect that a specific size doesn't work as getUserMedia succeeds but video stream may be 0x0 or 2x2)
            size.maxWidth = 2*size.width;
            size.maxHeight = 2*size.height;
        } else {
            size.maxWidth = size.width;
            size.maxHeight = size.height;
        }

        setMaxBandwidth();
        loadMediaSettings();
        monitorCPU();

        if (localStream && room) {
            logger.log("Reconnecting to room");
            room.connect();
            updateState('connecting');
            return;
        }
        
        if (videoDevices.length == 0 && audioDevices.length == 0 && (config.audio || config.video)) {
            var reason = "no_webcam";
            video.trigger("failed", reason);
            return;
        }
        
        if (!config.audio) {
            mediaSettings.computerAudio = false;
            if (config.video)
                config.audio = true;
        }

        var attribs = config.attributes;
        attribs = _.extend(attribs, {
            guestId: config.guestId,
            displayName: config.displayName ? config.displayName : "guest",
            hasCamera: config.video,
            sip: false,
            mediaSettings: mediaSettings,
            type: config.type
        });

        var accessTimer = setTimeout(function() {
            video.trigger('mediaRequested', config.video ? "video" : "audio");
        }, 200);

        localStream = Mcu.Stream({audio: config.audio, video: config.video, data: true, screen: false, size: size,
                                    attributes: attribs});

        localStream.addEventListener("access-accepted", function () {
            logger.log("Webcam access allowed");
            clearTimeout(accessTimer);
            video.trigger('mediaGranted');

            room = Mcu.Room({token: config.token, hasMedia: config.media});

            updateState('connecting');

            room.addEventListener("room-connected", function (roomEvent) {
                logger.log("Remote streams:", room.remoteStreams);

                var users = roomEvent.data.users;
                if (context.type == 'guest') {
                    var count = _.reduce(users, function(count, user) { return (user.role == 'guest' ? count+1 : count); }, 0);
                    if (count > config.options.maxGuests) {
                        reconnect = false;
                        stop(true);
                        video.trigger('failed', 'full');
                        return;
                    }
                } else {
                    if (roomEvent.data.appState)
                        setState(roomEvent.data.appState);
                }

                connected = true;
                subscribeToStreams(roomEvent.streams);

                updateState('connected');
            });

            room.addEventListener("room-disconnected", function (roomEvent) {
                logger.log("Disconnected from room");

                connected = false;
                monitorStop(localStream);

                _.each(reconnectTimers, function(timer) {
                    clearTimeout(timer);
                });

                if (reconnect) {
                    var reason = roomEvent.reason ? roomEvent.reason : '';
                    video.trigger("disconnected", reason);
                }
            });

            room.addEventListener("stream-subscribed", function(streamEvent) {
                var stream = streamEvent.stream;

                showVideo(stream, false);
                monitorStart(stream, false);

                updateState('subscribe', stream);
            });

            room.addEventListener("stream-added", function (streamEvent) {
                var stream = streamEvent.stream;

                subscribeToStreams([stream]);

                if (stream.getID() == localStream.getID()) {
                    // We are published to room
                    showVideo(localStream, true);
                    monitorStart(localStream, true);

                    published = PublishedFlags.StreamEstablished;
                    updateState('published', stream);

                    if (started) {
                        video.trigger('changeStream', stream.getID());
                    }
                } else {
                    // Someone else has published to room
                    updateState('remote_published', stream);
                }

                video.trigger('guestJoined', makeParticipantList(), stream.getID());
            });

            room.addEventListener("stream-removed", function (streamEvent) {
                var stream = streamEvent.stream;

                removeVideo(stream);
                delete subscribed[stream.getID()];

                if (stream.getID() == localStream.getID()) {
                    published = PublishedFlags.None;
                }

                video.trigger('guestLeft', makeParticipantList(), stream.getID());
            });

            room.addEventListener("stream-updated", function(streamEvent){
                var stream = streamEvent.stream;
                video.trigger('updatedParticipant', makeParticipantList(), makeParticipant(stream), streamEvent.msg);
                if (streamEvent.msg.hasCamera != undefined) {
                    logger.log("hasCamera changed");
                    changeVideo(stream);
                }
                if (streamEvent.msg.mediaSettings != undefined && streamEvent.msg.mediaSettings.muted != undefined) {
                    if (stream.getID() == localStream.getID() && streamEvent.msg.mediaSettings.muted != mediaSettings.muted) {
                        muteAudio(stream.getID(), streamEvent.msg.mediaSettings.muted, true);
                    }
                }
                if (stream.getID() == localStream.getID()) {
                    saveMediaSettings();
                }
                updateVideoControls(stream.getID());
            });

            room.addEventListener("stream-frozen", function (streamEvent) {
                var stream = streamEvent.stream;
                logger.log("Stream frozen: " + stream.getID());
                if (stream.getID() == localStream.getID()) {
                    restartVideo();
                }
            });

            room.connect();
        });

        localStream.addEventListener("access-denied", function (msg) {
            logger.log('Webcam access rejected');
            logger.flush();
            clearTimeout(accessTimer);
            var reason = msg.name == "PermissionDeniedError" ? "no_webcam" : "webcam_denied";
            video.trigger("failed", reason);
        });

        localStream.init();
    }

    /**
     * Stop current video streaming.  If close is true we also close/stop the local webcam stream.
     */
    function stop(close) {
        if (stopped) return;
        stopped = true;
        updateState('stopped');
        try {
            removeVideo(localStream);
            if (room && room.state > 0) {
                room.disconnect();
            }
            if (close) {
                localStream.close();
            }
        } catch (error) {
            logger.log("Reset error: " + error);
        }

        /* In case of exception, make one last attempt to close underlying stream */
        try {
            if (close && localStream) {
                localStream.stream.stop();
            }
        } catch (error) {
            logger.log("Reset error: " + error);
        }
        if (close) {
            logger.log('Local stream closed');
            if (room) room.removeAllListeners();
            if (localStream) localStream.removeAllListeners();
            room = null;
            localStream = null;
        }
    }

    /**
     * Get current room id.
     */
    function getRoom() {
        return room.roomID;
    }

    /**
     * Find participant info based on stream id (if stream id not specific local user's info is returned).
     */
    function getParticipant(streamId) {
        var stream = null;
        if (streamId === undefined || streamId === localStream.getID()) {
            stream = localStream;
        } else {
            stream = _.find(room.remoteStreams, function(s) { return s.getID() == streamId; });
        }
        return stream ? makeParticipant(stream) : null;
    }

    /**
     * Notify other guests of state change.
     */
    function sendDelta(saveValues, removeValues) {
        var msg = sendUpdates(false, 'delta', saveValues, removeValues);
        processDelta(msg);
    }

    /**
     * Expand/contract video player.
     * Fit within indicated element.
     */
    function toggleVideo(id, flag, container, forceExpand) {
        var expandFull = false;
        if (!isController() && displayMode != 'small' && videoMode != 'normal') {
            container = $("#room-body")[0];
            expandFull = true;
        } 
        /* Doing it using CSS only to avoid moving the element in the DOM, which was reported
           to cause problems when streaming. */
        if (flag) {
            if (!container || getStream(id) == null) {
                console.log("Expand video failed: container or stream not found");
                return;
            }
            if (expanded) {
                // Restore current expanded video
                toggleVideo(expanded.id, false, expanded.container);
            }
            if (!id) {
                id = localStream.getID();
            }
            var $elem = $("#player_" + id);
            var media = true;
            var $container = $(container);
            var participant = getParticipant(id);
            if (!config.media || (participant && !participant.hasCamera)) {
                // Participant, or us, does not have media so copy static image instead
                media = false;
                $elem = $("#vcont_"+id).find("div.video-static")
                    .clone().appendTo($container).addClass("copy")
                    .show();
            } else {
                showStaticImage(id, true);
                $elem.appendTo(container);
                try {
                    $elem.find("video")[0].play();
                } catch (e) {
                    logger.log("Failed to restart video");
                }
            }
            expanded = {id: id, container: container, forced: forceExpand, media: media};
            $elem
                .addClass('expanded')
                .css("position", "absolute")
                .css("text-align", "center")
                .offset($container.offset())
                .css("width", $container.width())
                .css("height", $container.height())
                .css("z-index", "99");
            resizeVideo(id, videoMode != 'normal' || displayMode == 'small');
            if (expandFull) {
                $("#video-bar").addClass("expand-full");
            }
            if (forceExpand) {
                $("#video-bar").find('.video-list').addClass('video-force');
            } else if (isController()) {
                var pos = getVideoPosition(id);
                if (pos) {
                    pos.top += 10;
                    pos.left += pos.width - 25;
                    $("#close-video-button").offset(pos);
                }
            }
            video.trigger('videoSize', id, 'expanded');
        } else {
            if (!expanded) return;
            if (!id) {
                id = expanded.id;
            }
            var $elem = $("#player_" + id);
            var $container = $(container);
            $("#close-video-button").css('left', -100);
            var participant = getParticipant(id);
            if (!expanded.media) {
                // No media, delete copy of static image
                $elem = $container.find('div.video-static.copy').remove();
            } else {
                $elem
                    .removeClass('expanded')
                    .css("position", "relative")
                    /*.css("top", "0")
                    .css("left", "0")*/
                    .css("width", "100%")
                    .css("height", "100%")
                    .css("z-index", "100");
                showStaticImage(id, false);
                $elem.appendTo("#vcont_" + id);
                try {
                    $elem.find("video")[0].play();
                } catch (e) {
                    logger.log("Failed to restart video");
                }
            }
            expanded = null;
            $("#video-bar").removeClass("expand-full");
            $("#video-bar").find('.video-list').removeClass('video-force');
            resizeVideo(id, true);
            video.trigger('videoSize', id, 'normal');
        }
        updateVideoControls(id);
    }

    /**
     * Get info about video player for indicated stream.
     */
    function getVideo(id) {
        var info = {
            container: "vcont_"+id,
            expanded: expanded && expanded.id == id
        };

        return info;
    }

    /**
     * Set output volume of all guests.
     * Volume is from 0 to 100.
     */
    function setVolume(vol) {
        if (app && app.setOutputGain) {
            var val = Math.exp(vol/22)/10;
            logger.log("Setting volume: " + val);
            app.setOutputGain(val);
        }
    }

    /**
     * Mute/un-mute selected participant.
     */
    function muteAudio(id, enable, quiet) {
        if (id == null || id == localStream.getID()) {
            var audio = localStream.stream;
            if (audio) {
                var track = audio.getAudioTracks()[0];
                if (track) {
                    track.enabled = !enable;
                    logger.log("Audio track:",!enable?"enabled":"disabled");
                }
                mediaSettings.muted = enable;
                if (quiet !== true)
                    updateAttributes(localStream.getID(), { mediaSettings: mediaSettings });
                saveMediaSettings();
                updateVideoControls();
                video.trigger('updatedSettings', 'muted');
            }
        } else if (room) {
            if (id == 'guests') {
                _.each(room.remoteStreams, function(s) {
                    if (s.getID() != localStream.getID() && !isController(s))
                        muteAudio(s.getID(), enable, quiet);
                });
            } else if (id == 'all') {
                _.each(room.remoteStreams, function(s) {
                    muteAudio(s.getID(), enable, quiet);
                });
            } else {
                var stream = room.remoteStreams[id];
                if (stream) {
                    var audio = stream.stream;
                    if (audio) {
                        var track = audio.getAudioTracks()[0];
                        if (track) {
                            track.enabled = !enable;
                            logger.log("Remote audio track:",!enable?"enabled":"disabled");
                        }
                    }
                }
                // Keep media streaming so we're ready to restart if the user requests it
                //room.muteAudio(stream, enable);
            }
        }
    }

    /**
     * Pause/un-pause guest videos.
     */
    function pauseVideo(id, enable) {
        if (id == null || id == localStream.getID()) {
            var video = localStream.stream;
            if (video) {
                var track = video.getVideoTracks()[0];
                if (track) {
                    track.enabled = !enable;
                    logger.log("Video track:",!enable?"enabled":"disabled");
                }
            }
        } else if (room) {
            if (id == 'guests') {
                _.each(room.remoteStreams, function(s) {
                    if (s.getID() != localStream.getID() && !isController(s))
                        pauseVideo(s.getID(), enable);
                });
                mediaSettings.guestVideo = !enable;
                saveMediaSettings();
            } else {
                var stream = room.remoteStreams[id];
                room.pauseVideo(stream, enable);
                pausedStreams[id] = enable;
                updateVideoControls(stream.getID());
            }
        }
    }

    /**
     * Get current media settings for stream.
     * { computerAudio: true|false, myVideo: true|false, guestVideo: true|false, boVideo: true|false, phonePause: true|false, guestPause: true|false }
     */
    function getMediaSettings(id) {
        if (id == null || id == localStream.getID()) {
            return mediaSettings;
        }
        return (room && room.remoteStreams[id]) ? room.remoteStreams[id].getAttributes().mediaSettings : null;
    }

    /**
     * Change media settings for stream.
     */
    function changeMediaSetting(id, setting, enabled) {
        if (id == null || id == localStream.getID()) {
            if (mediaSettings[setting] != enabled) {
                logger.log('Changing ' + setting + ' to ' + (enabled ? 'enabled' : 'disabled'));
                changeSetting[setting](enabled);
            }
            this.updateAttributes(localStream.getID(), { mediaSettings: mediaSettings });
            saveMediaSettings();
            return mediaSettings;
        } else {
            // TODO: change someone else's settings
        }
        return null;
    }

    /**
     * Resize expanded video player (e.g. because containing element has changed size).
     */
    function resize(width) {
        if (expanded) {
            setTimeout(function() { toggleVideo(expanded.id, true, expanded.container, expanded.forced) }, 100);
        }
        resizeVideoList(width);
    }

    /**
     * Update selected stream attributes.
     */
    function updateAttributes(streamId, attrs) {
        if (!room) return;
        var stream = room.remoteStreams[streamId];
        if (stream == undefined) stream = room.localStreams[streamId];
        if (stream !== undefined) {
            room.updateAttributes(stream,attrs);
        }
    }

    /**
     * Get stats (including audioLevel) for a stream.
     */
    function getStats(streamId) {
        if (!monitorStats[streamId])
            return null;
        return monitorStats[streamId];
    }

    /**
     * Play sound.
     */
    function playSound(file) {
        function localmute(flag) {
            var audio = localStream.stream;
            if (audio) {
                var track = audio.getAudioTracks()[0];
                if (track) {
                    track.enabled = !flag;
                }
            }
        }
        function localrestore() {
            if (!mediaSettings.muted)
                localmute(false);
        }

        logger.log("Playing sound:"+file);
        var snd = new Audio(file);
        snd.play();
        snd.addEventListener('playing', _.partial(localmute, true));
        snd.addEventListener('ended', localrestore);
        snd.addEventListener('error', localrestore);
    }

    /**
     * Send monitor command to server.
     */
    function monitor(options, callback) {
        if (options.streamId === undefined)
            options.streamId = localStream.getID();
        if (options.op == 'restart')
            restartVideo(options.streamId);
        else
            room.monitor(options, callback);
    }

    function trigger(event) {
        var that = this;
        var params = arguments;
        _.each(eventHandlers[event], function(handler) {
            handler.apply(that, params);
        });
    }

    function on(event, callback) {
        if (!eventHandlers[event])
            eventHandlers[event] = [];
        eventHandlers[event].push(callback);
    }

    var video = {
        init: init,
        start: start,
        stop: stop,
        getRoom: getRoom,
        getParticipant: getParticipant,
        sendDelta: sendDelta,
        toggleVideo: toggleVideo,
        getVideo: getVideo,
        setVolume: setVolume,
        muteAudio: muteAudio,
        pauseVideo: pauseVideo,
        getMediaSettings: getMediaSettings,
        changeMediaSetting: changeMediaSetting,
        resize: resize,
        updateAttributes: updateAttributes,
        getStats: getStats,
        playSound: playSound,
        monitor: monitor,
        trigger: trigger,
        on: on,
        bind: on
    };

    //_.extend(video, Backbone.Events);

    window.video = video;

})();

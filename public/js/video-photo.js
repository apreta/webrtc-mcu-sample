(function() {
    var captured     = false;
    var videoCount   = 0;

    navigator.getMedia = (navigator.getUserMedia ||
                          navigator.webkitGetUserMedia ||
                          navigator.mozGetUserMedia ||
                          navigator.msGetUserMedia);

    function refreshSources() {
        if (MediaStreamTrack && MediaStreamTrack.getSources) {
            MediaStreamTrack.getSources(function(sourceInfos) {
                for (var i = 0; i < sourceInfos.length; i++) {
                    if (sourceInfos[i].kind === 'video') {
                        videoCount++;
                    }
                }
            });
        }
    }

    function initialize(takeCallback) {
        var streaming    = false,
            video        = document.querySelector('#video'),
            canvas       = document.querySelector('#canvas'),
            photo        = document.querySelector('#photo'),
            startbutton  = document.querySelector('#startbutton'),
            width = 320,
            height = 0;

        if (videoCount > 0) return;

        var timer = setTimeout(function() {
            window.photo.dispatchEvent(new Event('mediaRequested'));
        }, 200);
        
        navigator.getMedia({video: true, audio: false},
            function(stream) {
              window.photo.stream = stream;  
              if (navigator.mozGetUserMedia) {
                video.mozSrcObject = stream;
              } else {
                var vendorURL = window.URL || window.webkitURL;
                video.src = vendorURL.createObjectURL(stream);
              }
              clearTimeout(timer);  
              window.photo.dispatchEvent(new Event('mediaGranted'));  
              refreshSources();
              video.play();
            },
            function(err) {
              clearTimeout(timer);  
              window.photo.dispatchEvent(new Event('mediaFailed'));  
              partytrace.log("Camera error occured! " + err);
            }
        );

        video.addEventListener('canplay', function(ev) {
            if (!streaming) {
              height = video.videoHeight / (video.videoWidth/width);
              video.setAttribute('width', width);
              video.setAttribute('height', height);
              canvas.setAttribute('width', width);
              canvas.setAttribute('height', height);
              streaming = true;
            }
        }, false);

        function takePicture() {
            if (videoCount > 0) {
                canvas.width = width;
                canvas.height = height;
                canvas.getContext('2d').drawImage(video, 0, 0, width, height);
                captured = true;
            }
        }

        startbutton.addEventListener('click', function(ev) {
            takePicture();
            ev.preventDefault();
            if (captured && takeCallback) takeCallback();
        }, false);
    };

    function getImageData() {
        if (!captured) return null;
        var data = canvas.toDataURL('image/jpeg');
        if (!data || data.length < 12) return null;
        return data;
    }

    function close() {
        try {
            if (window.photo.stream) {
                window.photo.stream.stop();
                window.photo.stream = null;
            }
        } catch (e) {
        }
    }
    
    function Emitter () {
        var eventTarget = document.createDocumentFragment();
        function delegate (method) {
            this[method] = eventTarget[method].bind(eventTarget);
        }
        Emitter.methods.forEach(delegate, this);
    }
    Emitter.methods = ["addEventListener", "dispatchEvent", "removeEventListener"];

    try {
        var photo = new Emitter();
        photo.initialize = initialize;
        photo.getImageData =  getImageData;
        photo.close = close;
        
        window.photo = photo;

        photo.addEventListener('mediaRequested', function() { console.log("webcam requested");});
        photo.addEventListener('mediaGranted', function() { console.log("webcam granted");});
        photo.addEventListener('mediaFailed', function() { console.log("webcam denied");});
    } catch (err) {
    }
    
})();
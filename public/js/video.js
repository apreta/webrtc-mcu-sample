(function() {
    "use strict";

    var config;
    var logger = console;

    var room;
    var localStream;
    var customRate = false;
    var startupTimer = null;

    /* Status flags */
    var connected = false;
    var started = false;
    var stopped = false;
    var reconnect = true;
    var PublishedFlags = { None:0, SentRequest:1, StreamEstablished:2 };
    var published = PublishedFlags.None;
    var masterState = false;
    var expanded = null;
    var mediaSettings = {
        computerAudio: true,    // speaker and mic disabled
        muted: false,           // mic muted
        myVideo: true,
        guestVideo: true
    };

    /* Keep track of who has subscribed to us */
    var subscribed = {};

    /* Track which streams we've paused locally */
    var pausedStreams = {};

    /* Current application state */
    var state = {};

    /* Stream display order */
    var streamDisplay = [];

    var useTimers = true;
    var publishTimer;
    var subscribeTimers = {};
    var reconnectTimers = [];

    var videoDevices = [];
    var audioDevices = [];

    var eventHandlers = {};

    var templates = {
        listTemplate: _.template("<div class='video-list' id='video-list'></div>"),
        videoTemplate: _.template("<div class='video-container <%= type %> <%= hasCamera ? \'webcam\' : \'nowebcam\' %> guest-<%= guestId %>' id='vcont_<%= id %>' style='width:320px; height:240px;'></div")
    };

    // Keep audio, video timeouts longer then server-side publisher frozen timer
    var STALLED_TIMEOUT = 30000;
    var LOST_VIDEO_TIMEOUT = 28000;
    var LOST_AUDIO_TIMEOUT = 26000;
    var NO_MEDIA_TIMEOUT = 10000;

    var videoConfig = {
        hd: {
            ratio: 9/16,
            invRatio: 16/9,
            width: 1280, height: 720
        },
        wide: {
            ratio: 9/16,
            invRatio: 16/9,
            width: 640, height: 360
        },
        normal: {
            ratio: 3/4,
            invRatio: 4/3,
            width: 640, height: 480
        },
        low: {
            ratio: 3/4,
            invRatio: 4/3,
            width: 320, height: 240
        }
    };
    var videoMode = "normal";

    var bandwidthConfig = {
        initial: { leaderMaxRate: 720, guestMaxRate: 240 },
        lowCpu1: { leaderMaxRate: 650, guestMaxRate: 200 },
        lowCpu2: { leaderMaxRate: 500, guestMaxRate: 100 }
    };
    var bandwidthSetting;


    /* Reset local state */
    function reset() {
        /* Leave local stream and room alone in case we are reconnecting */
        started = false;
        stopped = false;
        connected = false;
        published = PublishedFlags.None;
        masterState = false;
        expanded = null;
        subscribed = {};
        state = {};
        streamDisplay = [];
        reconnectTimers = [];
        // leave media settings as is
    }

    /* Build list of source devices (cameras, microphones, etc.) */
    /* N.B.: if called before webcam access is granted we won't get device names but should find out
      if user has a camera. */
    function refreshSources(callback) {
        videoDevices = [];
        audioDevices = [];

        if (window.MediaStreamTrack && MediaStreamTrack.getSources) {
            MediaStreamTrack.getSources(function(sourceInfos) {
                var audio_count = 0;
                var video_count = 0;
                for (var i = 0; i < sourceInfos.length; i++) {
                    var source = {
                        id: sourceInfos[i].id,
                        label: sourceInfos[i].label,
                        type: sourceInfos[i].kind
                    };
                    if (sourceInfos[i].kind === 'audio') {
                        audio_count++;
                        if (source.label === '') {
                            source.label = 'Audio ' + audio_count;
                        }
                        audioDevices.push(source);
                    } else {
                        video_count++;
                        if (source.label === '') {
                            source.label = 'Video ' + video_count;
                        }
                        videoDevices.push(source);
                    }
                }

                if (callback) callback();
            });
        } else {
            /*videoDevices.push({kind:'video',id:1});
            audioDevices.push({kind:'audio',id:2});*/
            if (callback) _.delay(callback);
        }
    }

    function getDeviceIds(audioId, videoId) {
        var devices = {};
        if (audioDevices.length > audioId) {
            devices.audioId = audioDevices[audioId].id;
        }
        if (videoDevices.length > videoId) {
            devices.videoId = videoDevices[videoId].id;
        }
        return devices;
    }

    function updateState(event, stream) {
        logger.log("** Event: " + event + (stream ? ", stream: " + stream.getID() : ""));
        var delayed = false;

        clearTimeout(startupTimer);
        startupTimer = setTimeout(function() {
            if (!started) {
                logger.log("Delay during startup, skipping to next step");
                updateState('delayed');
            }
        }, STALLED_TIMEOUT);

        switch (event) {
            case 'connecting':
                stopped = false;
                break;
            case 'connected':
                if (isLeader())
                    masterState = true;
                break;
            case 'published':
                clearTimeout(publishTimer);
                // Send my state to other guests
                _.each(room.remoteStreams, function(s) {
                    if (s.getID() != localStream.getID())
                        sendState(s);
                });
                break;
            case 'remote_published':
                // Send my/all state to new guest
                if (published) {
                    sendState(stream);
                }
                break;
            case 'subscribe':
                clearTimeout(subscribeTimers[stream.getID()]);
                break;
            case 'remote_subscribe':
                if (isLeader(stream)) {
                    masterState = true;
                }
                break;
            case 'stopped':
                stopped = true;
                clearTimeout(startupTimer);
                clearTimeout(publishTimer);
                _.each(subscribeTimers, function(t) {
                    clearTimeout(t);
                });
                break;
            case 'started':
                clearTimeout(startupTimer);
                break;
            case 'delayed':
                delayed = true;
                break;
        }

        if (stopped) {
            logger.log("Video stopped");
        }
        else if (!connected) {
            if (delayed) {
                logger.log("Unable to connect to room, trying again");
                stop(true);
            }
        }
        else if (published != PublishedFlags.StreamEstablished) {
            if (published != PublishedFlags.SentRequest) {
                publishStream();
            }
        }
        else if (!started) {
            // Note: we won't get here until our stream is published.  This shouldn't really be necessary,
            // but we don't show up in the participant list until that point.

            if (delayed || masterState || !findLeader()) {
                // We've received state from controller, or there is no controller, so start
                logger.log("All systems go");
                startEvent();
            }
        }
    }

    /* Restart video stream */
    function restartVideo(streamId) {
        logger.log("Restarting video:", streamId);
        if (streamId == undefined || streamId == localStream.getID()) {
            // ToDo: restart video without un-publishing
            unpublishStream();
            setTimeout(function() {
                if (published == PublishedFlags.None)
                    publishStream();
            }, 1000);
        } else {
            var stream = room.remoteStreams[streamId];
            if (stream) {
                unsubscribeStream(stream);
                setTimeout(function() {
                    // Make sure room & stream is still around
                    if (room && room.remoteStreams[streamId])
                        subscribeStream(stream);
                }, 2000);
            } else {
                logger.log("Remote stream not found");
            }
        }
    }

    function setMaxBandwidth(setting) {
        if (setting == undefined) {
            setting = bandwidthConfig.initial;
        }
        bandwidthSetting = setting;
        var rateCap = isLeader() ? setting.leaderMaxRate : setting.guestMaxRate;

        if (rateCap && window.app && window.app.setMaxBitRate) {
            customRate = true;
            logger.log("Set bit rate cap to " + rateCap);
            app.setMaxBitRate(rateCap);
        }
        // If not using viewer with setMaxBitRate API we can't dynamically adjust rate.
        // We could restart our stream, but adjusting the bitrate in that way is unlikely
        // to effect the CPU usage (the custom setMaxBitRate works by dropping frames
        // which does reduce CPU load, but there's no way to achieve the same thing in
        // an unmodified browser).
    }

    /* Process state update message */
    function processDelta(msg) {
        if (msg.updates) state = _.extend(state, msg.updates);
        if (msg.removes) state = _.omit(state, msg.removes);

        logger.log("Processing delta:", msg);
        logger.log("New state:", state);

        if (msg.to) {
            // A directed message indicates we've been subscribed to
            if (msg.to == localStream.getID()) {
                if (!subscribed[msg.src]) {
                    subscribed[msg.src] = true;

                    updateState('remote_subscribe', room.remoteStreams[msg.src]);
                }
            }
        } else if (started) {
            // Only forward app-created state changes to app
            video.trigger('stateUpdated', state);
        }
    }

    function setState(lastState) {
        var updates = {};
        _.each(lastState, function(v,k) {
            if (k.indexOf('global') === 0) {
                updates[k] = v;
            }
        });
        _.extend(state, updates);
    }

    /* GuestId 0 is the room leader */
    function isLeader(stream) {
        if (stream === undefined)
            return config.guestId == 0;
        else
            return stream.getAttributes().guestId == 0;
    }

    function findLeader() {
        return _.find(room.remoteStreams, function(s) { return s.getAttributes().guestId == 0; });
    }

    /* Will be undefined for sip call */
    function getGuestId(stream) {
        return stream.getAttributes().guestId;
    }

    /* Send update message */
    function sendUpdates(toStream, type, updates, removes) {
        if (removes === undefined) removes = [];
        var msg = {
            src: localStream.getID(),
            ctl: type,
            updates: updates,
            removes: removes
        };
        if (toStream) msg.to = toStream.getID();
        logger.log("Sending update:", msg);
        localStream.sendData(msg);
        return msg;
    }

    /* Send my state (or all state if we are controller) */
    function sendState(stream) {
        // Don't send state to sip streams.
        var guestId = getGuestId(stream);
        if (guestId == undefined)
            return;
        if (isLeader()) {
            logger.log("Sending full state to " + getGuestId(stream));
            sendUpdates(stream, 'initial', state);
        } else {
            logger.log("Sending state for " + config.guestId + " to " + getGuestId(stream));
            var me = config.guestId + ':';
            var updates = {};
            var send = false;
            _.each(state, function(v,k) {
                if (k.lastIndexOf(me, 0) === 0) {
                    updates[k] = v;
                    send = true;
                }
            });
            if (send) {
                sendUpdates(stream, 'delta', updates);
            }
        }
    }

    /* Send request message */
    function sendRequest(type, to) {
        var msg = {
            src: localStream.getID(),
            ctl: type,
            to: to
        }
        logger.log("Sending request:", msg);
        localStream.sendData(msg);
    }

    /* Make guest descriptor for a stream */
    function makeParticipant(stream) {
        var idx = _.indexOf(streamDisplay, stream.getID());
        return _.extend(stream.getAttributes(), {id: stream.getID(), displayIndex: idx >= 0 ? idx : 99 });
    }

    /* Make list of current participants */
    function makeParticipantList() {
        var list = _.filter(room.remoteStreams, function(s) { return !s.getAttributes().sip; });
        return _.map(list, function(s) { return makeParticipant(s); });
    }

    function getStream(streamId) {
        var stream = room.remoteStreams[streamId];
        if (stream == undefined) stream = room.localStreams[streamId];
        if (stream == undefined) stream = null;
        return stream;
    }

    function getStreamAttribute(streamId, field) {
        var stream = room.remoteStreams[streamId];
        if (stream == undefined) stream = room.localStreams[streamId];
        if (stream == undefined) return null;
        return stream.getAttributes()[field];
    }

    /* Subscribe to remote stream */
    function subscribeStream(stream) {
        if (stream.getAttributes().sip && !localStream.hasAudio())
            return;

        var guestId =  getGuestId(stream);
        logger.log("Subscribing to:", stream.getID(), "-", guestId != undefined ? guestId : 'sip');

        if (!stream.dataListener) {
            stream.addEventListener("stream-data", function (streamEvent) {
                var stream = streamEvent.stream;
                var msg = streamEvent.msg;
                logger.log("Received from " + getGuestId(stream) + ":", msg);
                switch (msg.ctl) {
                    case 'refresh':
                        if (!msg.to || msg.to == localStream.getID()) {
                            sendState(stream);
                        }
                        break;
                    case 'initial':
                    case 'delta':
                        processDelta(msg);
                        break;
                }
            });
            stream.dataListener = true;
        }

        room.subscribe(stream);
        if (stream.getAttributes().sip) {
            subscribed[stream.getID()] = true;
        }
        if (useTimers) {
            // Set up retry timer
            clearTimeout(subscribeTimers[stream.getID()]);
            subscribeTimers[stream.getID()] = setTimeout(function () {
                logger.log("Subscribe timeout: "+stream.getID());
                restartVideo(stream.getID());
            }, STALLED_TIMEOUT);
        }
    }

    function unsubscribeStream(stream) {
        removeVideo(stream);
        room.unsubscribe(stream);
        clearTimeout(subscribeTimers[stream.getID()]);
    }

    /* Subscribe to list of streams (i.e. current room guests) */
    function subscribeToStreams(streams) {
        for (var index in streams) {
            var stream = streams[index];
            if (localStream.getID() !== stream.getID()) {
                subscribeStream(stream);
            }
        }
    }

    /* Publish my stream to room */
    function publishStream() {
        if (!published) {
            localStream.getAttributes().hasCamera = config.video && videoDevices.length > 0;
            logger.log("Publishing local stream:", localStream.getAttributes());
            var br = 512;
            room.publish(localStream, customRate ? {} : {maxVideoBW: br});
            published = PublishedFlags.SentRequest;

            // Set up retry timer
            if (useTimers) {
                clearTimeout(publishTimer);
                publishTimer = setTimeout(function() {
                    if (published != PublishedFlags.StreamEstablished) {
                        logger.log("Publish timeout");
                        restartVideo(localStream.getID());
                    }
                }, STALLED_TIMEOUT);
            }
        }
    }

    function unpublishStream() {
        if (published) {
            logger.log("Unpublishing local stream");
            localStream.priorId = localStream.getID();
            removeVideo(localStream);
            room.unpublish(localStream);
            // room.unpublish seems to clear the stream id so we can't recognize our stream in the event handler so
            // clear published flag now
            published = PublishedFlags.None;
            clearTimeout(publishTimer);
        }
    }

    function startEvent() {
        if (!started) {
            started = true;
            updateState('started');
            video.trigger('started', makeParticipantList(), state);
        }
    }

    /* Create video player and show stream */
    function showVideo(stream, local) {
        var parent = $('#video-list');
        var sid = stream.getID();
        var attribs = stream.getAttributes();
        var avatar = attribs.avatar;
        if (attribs.sip) {
            parent.prepend(templates.videoTemplate({id: sid, guestId: -1, type: "phone",
                avatar: '', name: '', controller: false, hasCamera: false}));
        } else {
            var content = templates.videoTemplate({id: sid, guestId: attribs.guestId, type: (local ? "local" : "remote"),
                    avatar: avatar, name: attribs.displayName, leader: isLeader(), hasCamera: attribs.hasCamera});
            // See if we should replace an existing video container (e.g. stream is reconnecting)
            var curr = parent.find('.video-container.guest-'+attribs.guestId);
            if (curr.length) {
                curr.replaceWith(content);
            } else if (isLeader(stream)) {
                curr = parent.find(".video-container.guest-"+config.guestId);
                if (curr.length) {
                    curr.after(content);
                } else {
                    parent.prepend(content);
                }
            } else {
                parent.append(content);
            }
        }
        if (attribs.guestId != undefined && reconnectTimers[attribs.guestId]) {
            clearTimeout(reconnectTimers[attribs.guestId]);
            delete reconnectTimers[attribs.guestId];
        }

        //updateVideoControls(sid);

        /*$("#video_mute_"+sid).click(function(e) {
            var set = $(this).is('.enabled');
            var settings = getStreamAttribute(sid, "mediaSettings");
            if (settings.computerAudio)
                updateAttributes(sid, { mediaSettings: { muted: !set } });
        });

        $("#video_expand_"+sid).click(function(e) {
            if (!$(this).hasClass('disabled'))
                video.trigger('expandClick', sid);
        });

        $("#video_pause_"+sid).click(function(e) {
            if (!$(this).hasClass('disabled')) {
                var enable = !$(this).is('.enabled');
                pauseVideo(sid, enable);
            }
        });*/

        stream.show('vcont_'+sid, local ? {muted: true, media: config.media, className: 'video_player local', bar: false} :
            {media: config.media, className:'video_player', bar: false});
        streamDisplay = _.union(streamDisplay, sid);

        //resizeVideo(sid, true);
        //resizeVideoList();

        // If user has no camera we substitute their avatar (probably initials)
        if (!attribs.hasCamera || !config.media) {
            showStaticImage(sid, true);
        }

        /*
        setTimeout(function() {
            if (!mediaSettings.guestVideo && !isLeader(stream) && !local)
                pauseVideo(sid, true);
            if (!mediaSettings.boVideo && isLeader(stream))
                pauseVideo(sid, true);
            if (!mediaSettings.computerAudio || (local && mediaSettings.muted))
                muteAudio(sid, true);
            if (expanded && expanded.id == sid)
                toggleVideo(expanded.id, true, expanded.container, expanded.forced);
        }, 2000);
        */
    }

    /* Stream's hasCamera flag has changed */
    function changeVideo(stream) {
        if (stream.getAttributes().hasCamera) {
            $('#vcont_' + stream.getID()).removeClass('nowebcam').addClass('webcam');
            showStaticImage(stream.getID(), false);
        } else {
            $('#vcont_' + stream.getID()).removeClass('webcam').addClass('nowebcam');
            // If user has no camera we substitute their avatar (probably initials)
            showStaticImage(stream.getID(), true);
        }
    }

    /* Set width of solid color div to create volume level indicator */
    /*
    function showVolume(stream, vol) {
        var mediaSettings = stream.getAttributes().mediaSettings;
        if (mediaSettings.muted || mediaSettings.computerAudio == false) vol = 0;
        if (started)
            video.trigger('audioLevel', stream.getID(), vol);
        vol = vol/32767 * videoWidth;
        $(stream.volumeElem).width(Math.floor(vol));
    }
    */

    /* Remove video element */
    function removeVideo(stream) {
        logger.log("Removing video: " + stream.getID());

        // Collapse if expanded
        if (expanded && expanded.id == stream.getID()) {
            toggleVideo(expanded.id, false, expanded.container);
        }

        try {
            streamDisplay = _.filter(streamDisplay, function(id) { return id != stream.getID(); });

            var remove = function() {
                // Remove stream from DOM
                if (stream.elementID !== undefined) {
                    var element = document.getElementById(stream.elementID);
                    var parent = document.getElementById('video-list');
                    parent.removeChild(element);
                    //resizeVideoList();
                }
            };

            if (!stopped) {
                //$("#vcont_"+stream.getID()).find(".video-reconnect").show();
                if (stream.getID() != localStream.getID()) {
                    var guestId = stream.getAttributes().guestId;
                    if (guestId != undefined) {
                        reconnectTimers[guestId] = setTimeout(function() {
                            logger.log("Timed out video:" + guestId);
                            remove();
                        }, 15000);
                    }
                }
            } else {
                remove();
            }

        } catch (error) {
            logger.log("Remove video error: " + error);
        }
    }


    /**
     * Initialize video controller.
     */
    function init(_config) {
        //L.Logger.log = log;

        if (_config !== undefined) {
            config = _config;
            // Allow inbound media even if webcam/mic not present
            config.media = (config.media && navigator.webkitGetUserMedia !== undefined);
        }
        reset();
        logger.log("Token:" + config.token);

        $('#'+config.videoContainerId).empty().append(templates.listTemplate({}));

        if (config.media) {
            refreshSources(function() {
                // If no camera try to continue with just mic.
                config.video = config.video && videoDevices.length > 0;
                //config.audio = config.audio && audioDevices.length > 0;
                if (videoDevices.length == 0) { logger.log('No webcam found'); }
                if (audioDevices.length == 0) { logger.log('No audio found'); }
                _.delay(function() { video.trigger('initialized'); });
            });
        } else {
            logger.log("Ignoring any devices");
            videoDevices = [];
            audioDevices = [];
            _.delay(function() { video.trigger('initialized'); });
        }
    }

    /**
     *  Start streaming and event processing.
     */
    function start() {
        var size = videoConfig[videoMode];
        size.minWidth = size.width;
        size.minHeight = size.height;
        if (config.size == 'small') {
            // Android phones seem to have more limited support so give it a range
            // (it's hard to detect that a specific size doesn't work as getUserMedia succeeds but video stream may be 0x0 or 2x2)
            size.maxWidth = 2*size.width;
            size.maxHeight = 2*size.height;
        } else {
            size.maxWidth = size.width;
            size.maxHeight = size.height;
        }

        setMaxBandwidth();

        if (localStream && room) {
            logger.log("Reconnecting to room");
            room.connect();
            updateState('connecting');
            return;
        }

        if (videoDevices.length == 0 && audioDevices.length == 0 && (config.audio || config.video)) {
            var reason = "no_webcam";
            video.trigger("failed", reason);
            return;
        }

        if (!config.audio) {
            mediaSettings.computerAudio = false;
            if (config.video)
                config.audio = true;
        }

        var attribs = config.attributes;
        attribs = _.extend(attribs, {
            guestId: config.guestId,
            displayName: config.displayName ? config.displayName : "guest",
            hasCamera: config.video,
            sip: false,
            mediaSettings: mediaSettings,
            type: config.type
        });

        var accessTimer = setTimeout(function() {
            video.trigger('mediaRequested', config.video ? "video" : "audio");
        }, 200);

        localStream = Mcu.Stream({audio: config.audio, video: config.video, data: true, screen: false, size: size,
                                    attributes: attribs});

        localStream.addEventListener("access-accepted", function () {
            logger.log("Webcam access allowed");
            clearTimeout(accessTimer);
            video.trigger('mediaGranted');

            room = Mcu.Room({token: config.token, hasMedia: config.media});

            updateState('connecting');

            room.addEventListener("room-connected", function (roomEvent) {
                logger.log("Remote streams:", room.remoteStreams);

                var users = roomEvent.data.users;
                if (context.type == 'guest') {
                    var count = _.reduce(users, function(count, user) { return (user.role == 'guest' ? count+1 : count); }, 0);
                    if (count > config.options.maxGuests) {
                        reconnect = false;
                        stop(true);
                        video.trigger('failed', 'full');
                        return;
                    }
                } else {
                    if (roomEvent.data.appState)
                        setState(roomEvent.data.appState);
                }

                connected = true;
                subscribeToStreams(roomEvent.streams);

                updateState('connected');
            });

            room.addEventListener("room-disconnected", function (roomEvent) {
                logger.log("Disconnected from room");

                connected = false;

                _.each(reconnectTimers, function(timer) {
                    clearTimeout(timer);
                });

                if (reconnect) {
                    var reason = roomEvent.reason ? roomEvent.reason : '';
                    video.trigger("disconnected", reason);
                }
            });

            room.addEventListener("stream-subscribed", function(streamEvent) {
                var stream = streamEvent.stream;

                showVideo(stream, false);

                updateState('subscribe', stream);
            });

            room.addEventListener("stream-added", function (streamEvent) {
                var stream = streamEvent.stream;

                subscribeToStreams([stream]);

                if (stream.getID() == localStream.getID()) {
                    // We are published to room
                    showVideo(localStream, true);

                    published = PublishedFlags.StreamEstablished;
                    updateState('published', stream);

                    if (started) {
                        video.trigger('changeStream', stream.getID());
                    }
                } else {
                    // Someone else has published to room
                    updateState('remote_published', stream);
                }

                video.trigger('guestJoined', makeParticipantList(), stream.getID());
            });

            room.addEventListener("stream-removed", function (streamEvent) {
                var stream = streamEvent.stream;

                removeVideo(stream);
                delete subscribed[stream.getID()];

                if (stream.getID() == localStream.getID()) {
                    published = PublishedFlags.None;
                }

                video.trigger('guestLeft', makeParticipantList(), stream.getID());
            });

            room.addEventListener("stream-updated", function(streamEvent){
                var stream = streamEvent.stream;
                video.trigger('updatedParticipant', makeParticipantList(), makeParticipant(stream), streamEvent.msg);
                if (streamEvent.msg.hasCamera != undefined) {
                    logger.log("hasCamera changed");
                    changeVideo(stream);
                }
                if (streamEvent.msg.mediaSettings != undefined && streamEvent.msg.mediaSettings.muted != undefined) {
                    if (stream.getID() == localStream.getID() && streamEvent.msg.mediaSettings.muted != mediaSettings.muted) {
                        muteAudio(stream.getID(), streamEvent.msg.mediaSettings.muted, true);
                    }
                }
                //updateVideoControls(stream.getID());
            });

            room.addEventListener("stream-frozen", function (streamEvent) {
                var stream = streamEvent.stream;
                logger.log("Stream frozen: " + stream.getID());
                if (stream.getID() == localStream.getID()) {
                    restartVideo();
                }
            });

            room.connect();
        });

        localStream.addEventListener("access-denied", function (msg) {
            logger.log('Webcam access rejected');
            logger.flush();
            clearTimeout(accessTimer);
            var reason = msg.name == "PermissionDeniedError" ? "no_webcam" : "webcam_denied";
            video.trigger("failed", reason);
        });

        localStream.init();
    }

    /**
     * Stop current video streaming.  If close is true we also close/stop the local webcam stream.
     */
    function stop(close) {
        if (stopped) return;
        stopped = true;
        updateState('stopped');
        try {
            removeVideo(localStream);
            if (room && room.state > 0) {
                room.disconnect();
            }
            if (close) {
                localStream.close();
            }
        } catch (error) {
            logger.log("Reset error: " + error);
        }

        /* In case of exception, make one last attempt to close underlying stream */
        try {
            if (close && localStream) {
                localStream.stream.stop();
            }
        } catch (error) {
            logger.log("Reset error: " + error);
        }
        if (close) {
            logger.log('Local stream closed');
            if (room) room.removeAllListeners();
            if (localStream) localStream.removeAllListeners();
            room = null;
            localStream = null;
        }
    }

    /**
     * Get current room id.
     */
    function getRoom() {
        return room.roomID;
    }

    /**
     * Find participant info based on stream id (if stream id not specific local user's info is returned).
     */
    function getParticipant(streamId) {
        var stream = null;
        if (streamId === undefined || streamId === localStream.getID()) {
            stream = localStream;
        } else {
            stream = _.find(room.remoteStreams, function(s) { return s.getID() == streamId; });
        }
        return stream ? makeParticipant(stream) : null;
    }

    /**
     * Notify other guests of state change.
     */
    function sendDelta(saveValues, removeValues) {
        var msg = sendUpdates(false, 'delta', saveValues, removeValues);
        processDelta(msg);
    }

    /**
     * Set event to video listeners.
     */
    function trigger(event) {
        var that = this;
        var params = arguments;
        _.each(eventHandlers[event], function(handler) {
            handler.apply(that, params);
        });
    }

    /**
     * Register for video events.
     */
    function on(event, callback) {
        if (!eventHandlers[event])
            eventHandlers[event] = [];
        eventHandlers[event].push(callback);
    }


    var video = {
        init: init,
        start: start,
        stop: stop,
        getRoom: getRoom,
        getParticipant: getParticipant,
        sendDelta: sendDelta,
        trigger: trigger,
        on: on,
        bind: on
    };

    window.video = video;

})();

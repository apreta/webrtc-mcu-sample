#!/bin/sh

cmd=$1
if [ -z "$cmd" ]; then cmd='start' ; fi

if ! /usr/local/sbin/rabbitmqctl status ; then
	if [ "$cmd" = "start" ]; then
		/usr/local/sbin/rabbitmq-server &
	fi
else
	if [ "$cmd" = "stop" ]; then
		/usr/local/sbin/rabbitmqctl stop
	fi
fi

if [ "$cmd" = "start" ]; then
	mongod --dbpath /usr/local/var/mongodb --logpath /usr/local/var/log/mongodb/mongo.log --fork
else
	if [ "$cmd" = "stop" ]; then
	    killall mongod
	fi
fi	

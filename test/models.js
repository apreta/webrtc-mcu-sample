var chai = require('chai');
var should = chai.should();
var User = require('../models/User');
var Token = require('../models/Token');

describe('User Model', function() {
  it('should create a new user', function(done) {
    var user = new User({
      email: 'test@gmail.com',
      password: 'password'
    });
    user.save(function(err) {
      if (err) return done(err);
      done();
    });
  });

  it('should not create a user with the unique email', function(done) {
    var user = new User({
      email: 'test@gmail.com',
      password: 'password'
    });
    user.save(function(err) {
      if (err) err.code.should.equal(11000);
      done();
    });
  });

  it('should find user by email', function(done) {
    User.findOne({ email: 'test@gmail.com' }, function(err, user) {
      if (err) return done(err);
      user.email.should.equal('test@gmail.com');
      done();
    });
  });

  it('should delete a user', function(done) {
    User.remove({ email: 'test@gmail.com' }, function(err) {
      if (err) return done(err);
      done();
    });
  });
});

describe('Token Model', function() {
  var session = Date.now();

  it('should create a new token', function(done) {
    var token = new Token({roomId:2,guestId:0,session:session});
    token.save(function(err, token) {
      if (err) return done(err);
      token.session.should.equal(''+session);
      done();
    });
  });

  it('should find token', function(done) {
    var token = Token.findByDetails(2,0,session,function(err,token) {
      if (err) return done(err);
      token.roomId.should.equal('2');
      done();
    })
  });

  it('should not find token', function(done) {
    var token = Token.findByDetails(2,1,session,function(err,token) {
      if (err) return done(err);
      should.not.exist(token);
      done();
    })
  });

});
